#!/usr/bin/env zsh

##################################################################################################
#                                                                                                #
#    Perform any post-configuration steps that I haven't bothered figuring out                   #
#    how to script out (yet) without logging back in                                             #
#                                                                                                #
##################################################################################################


# Check if a command exists
#
check_cmd() {
  command -v "$1" > /dev/null 2>&1
}

# Install some recent versions of Python
#
setup_python() {

  declare -a py_versions=(
    "3.7"
    "3.8"
    "3.9"
    "3.10"
    "3.11"
    "3.12"
  )

  # TODO: Remove --quiet?
  configure_opts="--enable-loadable-sqlite-extensions --enable-optimizations"
  make_opts="--quiet --jobs=$(($(nproc) + 1))"
  make_install_opts="--quiet --jobs=$(($(nproc) + 1))"

  # TODO: This never finished running when I finally got to test it
  # CONFIGURE_OPTS=$configure_opts MAKE_OPTS=$make_opts MAKE_INSTALL_OPTS=$make_install_opts pyenv install "${py_version}"

  # Update available versions before trying to install
  pyenv update

  for py_version in "${py_versions[@]}"; do

    if pyenv install -l | grep -q "$py_version"; then
      echo -e "\n[$(date)] Installing Python version ${py_version}..."
      CONFIGURE_OPTS=$configure_opts pyenv install "${py_version}"
      echo -e "\n[$(date)] Done installing Python version ${py_version}!"

    else
      echo -e "\n[$(date)] ERROR! Failed to find Python version ${py_version}!" 1>&2

    fi

  done

  echo -e "\n[$(date)] Done installing all the Python versions! Remember to set 'pyenv global 3.x.x' and then install packages via 'requirements*.txt'!"

}

# Install latest LTS versions as of writing
# NOTE: Unused, but keeping just in case I like this better
#
setup_nodenv() {

  echo -e "\n[$(date)] Installing Node.js versions 12.22.10, 14.19.0, and 16.4.0..."

  nodenv install 12.22.10
  nodenv install 14.19.0
  nodenv install 16.4.0

  echo -e "\n[$(date)] Done installing Python versions! Remember to set 'nodenv global x.x.x'!"

}

# Install latest LTS version as of writing
#
# TODO: Migrate to install step since this no longer requires logging back in
#
setup_nvm() {

  export NVM_DIR="$HOME/.nvm"
  [ -s "$NVM_DIR/nvm.sh" ] && \. "$NVM_DIR/nvm.sh"

  nvm install --lts
  nvm use --lts
  npm install --global yarn

}

# Install go 1.17 (apparently can't just specify --latest or similar...)
#
setup_gvm() {

  # For reference: need to bootstrap go with an older version first if we're installing from source
  # TODO: Can we actually skip since there's alrady a system installation?
  # gvm install go1.4 -B
  # gvm use go1.4
  # export GOROOT_BOOTSTRAP=$GOROOT

  # Install the target version
  gvm install go1.17 --prefer-binary
  gvm use go1.17 --default

}

# Install NVChad config for neovim
#
# NOTE: Need to run nvim for the installation to fully take place
#
configure_neovim(){
  git clone https://github.com/NvChad/starter ~/.config/nvim
}

main() {

  echo -e "\n[$(date)] Installing base utilities. This will take a while..."

  if check_cmd pyenv; then
    setup_python
  else
    echo -e "\n[$(date)] pyenv isn't on PATH!"
  fi

  if check_cmd nvm; then
    setup_nvm
  else
    echo -e "\n[$(date)] nvm isn't on PATH!"
  fi

  if check_cmd gvm; then
    setup_gvm
  else
    echo -e "\n[$(date)] gvm isn't on PATH!"
  fi

  echo -e "\n[$(date)] Done!"

}


# ! Disabled so that this can be run piecemeal
# main
