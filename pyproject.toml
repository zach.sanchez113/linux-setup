# This is purely for controlling this repo's config
# See 'templates/' for a proper template

[tool.ruff]
# Use Python 3.11
target-version = "py311"

# Exclude a variety of commonly ignored directories.
exclude = [
  ".bzr",
  ".direnv",
  ".eggs",
  ".git",
  ".git-rewrite",
  ".hg",
  ".mypy_cache",
  ".nox",
  ".pants.d",
  ".pytype",
  ".ruff_cache",
  ".svn",
  ".tox",
  ".venv",
  "__pypackages__",
  "_build",
  "buck-out",
  "build",
  "dist",
  "node_modules",
  "venv",
]

# Same as Black.
line-length = 110
indent-width = 4

[tool.ruff.lint]
select = [
  ## pycodestyle errors (E)
  # Issues with imports
  "E4",
  # Basic syntax issues, e.g. `x == None` instead of `x is None`
  "E7",
  # E902: Issue w/ dev setup, e.g. can't read a file for some reason
  # E999: Syntax errors in code
  "E9",

  ## pyflakes (F)
  "F",

  ## isort (I)
  "I",

  ## pep8-naming (N)
  # Naming conventions
  "N8",
  # Invalid module name
  "N9",

  ## pydocstyle (D)
  # Spacing and indentation
  "D2",
  # Double quotes + escape if needed
  "D3",
  # Undocumented argument
  "D417",
  # Docstring is empty
  "D419",

  # pyupgrade (UP)
  "UP",

  # flake8-2020
  "YTT",

  # flake8-async
  # Check for bad async/asyncio practices
  "ASYNC",

  # flake8-trio
  # Check for bad async/asyncio practices
  # NOTE: Preview functionality as of writing
  # "TRIO",

  ## flake8-bandit
  # Don't use `exec`...
  "S102",
  # try/except/pass
  "S110",
  # Suspicious pickle usage
  "S301",
  # Suspicious marshal usage
  "S302",
  # Suspicious eval usage
  "S307",
  # More obviously bad
  "S5",
  # Jinja and Mako template issues
  "S7",

  # flake8-bugbear (potential sources of bugs)
  "B",

  # flake8-builtins (checks for shadowing)
  "A",

  # flake8-comprehensions (remove unnecessary comprehensions)
  "C4",

  # flake8-django
  "DJ",

  ## flake8-implicit-str-concat (implicit string concatenation is bad)
  # Single line
  "ISC001",
  # Multi line
  "ISC002",

  # flake8-import-conventions
  "ICN",

  # flake8-no-pep420 (missing __init__.py)
  "INP",

  # flake8-print
  "T20",

  ## flake8-return (check for issues w/ return statements)
  # TODO: Noise or not?
  #
  # Don't explicitly return None if it's the only possible return value
  "RET501",
  # Explicitly return None if it's not the only possible return value
  "RET502",
  # Missing explicit return at the end of function able to return non-None value
  "RET503",
  # Unnecessary variable assignment before return
  # "RET504",
  # Unnecessary else after return
  # "RET505",
  # Unnecessary else after raise
  # "RET506",
  # Unnecessary else after continue
  # "RET507",
  # Unnecessary else after break
  # "RET508",

  # flake8-simplify
  #
  # The parser seems to be relatively basic, so this plugin generates a lot of noise. Only enable the rules
  # that don't require advanced parsing.
  #
  # Duplicate isinstance
  "SIM101",
  # Return condition directly instead of using if statement to return True/False
  "SIM103",
  # Use contextlib.suppress instead of try/except/pass
  "SIM105",
  # Use {replacement} instead of for loop
  "SIM110",
  # Use context manager when opening files
  "SIM115",
  # Don't nest context managers
  "SIM117",
  # Use key in dict, not key in dict.keys()
  "SIM118",
  # Simplify boolean expressions + avoid doing weird things with booleans
  "SIM2",
  # Yoda conditions, e.g. `if "Foo" == foo` (this is a carryover from other languages, but has no benefit in Python)
  "SIM3",
  # Use `dict.get(key, default)` instead of `if key in dict: value = dict[key]; else value = default`
  "SIM4",
  # Use `dict.get(key)` instead of `if key in dict: value = dict[key]; else value = None`
  "SIM9",

  ## flake8-type-checking
  # Check for runtime imports in a type-checking block (NameError at runtime)
  "TCH004",
  # Empty type-checking block
  "TCH005",

  ## flake8-todos
  # For consistency, use TODO instead of FIXME or BUG
  "TD001",
  # Missing colon in TODO
  "TD004",
  # Missing description in TODO
  "TD005",
  # Invalid TODO capitalization
  "TD006",
  # Missing space after colon in TODO
  "TD007",

  # eradicate (remove commented-out code)
  #
  # This looks useful, but you can get a fair few false positives from comments that explain something with
  # code snippets/examples
  #
  # "ERA",

  # pylint
  # TODO: There are a ton of rules, look into them later
  # "PL",

  ## tryceratops (exception handling antipatterns)
  # TypeError should be used if type check fails
  "TRY004",
  # Use `raise` instead of `raise e` if an exception is being re-raised
  "TRY201",
  # Don't use return statements in try blocks, use try/except/else instead
  "TRY300",
  # Don't immediately re-raise caught exceptions
  "TRY302",
  # Use logging.exception (properly) instead of logging.error
  "TRY4",

  ## Ruff-specific rules
  # General code quality issues
  # WARNING: the automatic fix for RUF015 is not safe
  "RUF0",
  # Validate pyproject.toml
  "RUF200",
]

ignore = [
  ## NOTE: Might want to uncomment depending on the project
  # Non-lowercase variable in function
  # "N806",
  # Mixed case variable in class scope
  # "N815",

  # Unused variable
  "F841",

  # Allow blank lines after function docstrings because it looks cleaner imo
  "D202",

  # Doesn't like docstring summary that's more than one line
  "D205",

  # Complains about the UTF-8 pragma we need in models.py
  "UP009",

  # Complains about using functions like dict() instead of the literal equivalent, which is a matter of
  # preference
  "C408",
  "C409",
  "C410",

  # Unused loop control variable
  # TODO: Use or not?
  # "B007",
]

# Allow fix for all enabled rules (when `--fix`) is provided.
fixable = ["ALL"]
unfixable = []

# Allow unused variables when underscore-prefixed.
dummy-variable-rgx = "^(_+|(_+[a-zA-Z0-9_]*[a-zA-Z0-9]+?))$"

# Ignored unused imports in __init__.py
ignore-init-module-imports = true

[tool.ruff.lint.isort]
# isort is a Python utility / library to sort imports alphabetically, and automatically separated into
# sections and by type.
#
# Documentation: https://pycqa.github.io/isort/

# This is sometimes required for type annotations
required-imports = ["from __future__ import annotations"]

[tool.ruff.lint.pydocstyle]
convention = "google"

[tool.ruff.format]
# Like Black, use double quotes for strings.
quote-style = "double"

# Like Black, indent with spaces, rather than tabs.
indent-style = "space"

# Like Black, respect magic trailing commas.
skip-magic-trailing-comma = false

# Like Black, automatically detect the appropriate line ending.
line-ending = "auto"

[tool.pyright]
# Pyright is a fast type checker meant for large Python source bases.
# It can run in a "watch" mode and performs fast incremental updates when files are modified.
#
# Repository: https://github.com/microsoft/pyright
# Configuration reference: https://github.com/microsoft/pyright/blob/main/docs/configuration.md

extraPaths = []
pythonPlatform = "All"
pythonVersion = "3.10"
stubPath = "./types"
typeCheckingMode = "strict"
useLibraryCodeForTypes = true
verboseOutput = true

# Diagnostic severity overrides
# Not all errors are created equal!
reportAssertAlwaysTrue = "information"
reportCallInDefaultInitializer = "warning"
reportConstantRedefinition = "warning"
reportDuplicateImport = "error"

# False positives for pretty common operations
reportFunctionMemberAccess = "none"

reportGeneralTypeIssues = "warning"
reportImplicitStringConcatenation = "warning"
reportImportCycles = "warning"
reportIncompatibleMethodOverride = "warning"
reportIncompatibleVariableOverride = "warning"
reportIncompleteStub = "information"
reportInconsistentConstructor = "warning"
reportInvalidStringEscapeSequence = "error"
reportInvalidStubStatement = "information"

# False positives due to third-party modules
reportUnknownParameterType = "none"
reportUnknownArgumentType = "none"
reportUnknownLambdaType = "none"
reportUnknownVariableType = "none"
reportUnknownMemberType = "none"

reportMissingParameterType = "warning"
reportMissingTypeArgument = "warning"
reportInvalidTypeVarUse = "error"
reportMissingImports = "warning"
reportMissingModuleSource = "information"
reportMissingTypeStubs = "none"
reportOptionalCall = "warning"
reportOptionalContextManager = "warning"
reportOptionalIterable = "warning"
reportOptionalMemberAccess = "warning"
reportOptionalOperand = "warning"
reportOptionalSubscript = "warning"
reportOverlappingOverload = "warning"
reportPrivateImportUsage = "information"
reportPrivateUsage = "information"
reportPropertyTypeMismatch = "warning"
reportSelfClsParameterName = "warning"
reportTypedDictNotRequiredAccess = "warning"
reportUnboundVariable = "error"
reportUndefinedVariable = "error"
reportUnnecessaryCast = "warning"
reportUnnecessaryComparison = "warning"
reportUnnecessaryIsInstance = "information"
reportUnnecessaryTypeIgnoreComment = "information"
reportUnsupportedDunderAll = "error"
reportUntypedBaseClass = "information"
reportUntypedClassDecorator = "information"

# False positives due to third-party modules
reportUntypedFunctionDecorator = "none"

reportUntypedNamedTuple = "information"

# This is super annoying since it'll almost always be an obvious bug anyway if it matters
reportUnusedCallResult = "none"

reportUnusedClass = "warning"
reportUnusedCoroutine = "warning"
reportUnusedFunction = "warning"
reportUnusedImport = "warning"
reportUnusedVariable = "warning"
reportWildcardImportFromLibrary = "warning"
