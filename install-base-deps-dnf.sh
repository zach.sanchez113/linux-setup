#!/usr/bin/env bash

##################################################################################################
#                                                                                                #
#    Install various dependencies via dnf, i.e. various utilities, libraries, etc.               #
#                                                                                                #
##################################################################################################


sudo dnf install dnf-plugins-core epel-release
sudo dnf config-manager --set-enabled powertools

# NOTE: Check 'dnf grouplist --hidden' to see some of these
sudo dnf groupinstall -y --with-optional \
  "Container Management" \
  "Development Tools" \
  "System Tools" \
  "Networking Tools"

sudo dnf groupinstall -y \
  "Headless Management" \
  "System Tools" \
  "Additional Development" \
  "Network File System Client"

declare -a rpm_deps=(
  "automake"
  "binutils"
  "btrfs-progs-devel"
  "bzip2"
  "bzip2-devel"
  "clang-tools-extra"
  "cmake"
  "conmon"
  "containernetworking-plugins"
  "containers-common"
  "crun"
  "curl"
  "device-mapper-devel"
  "gcc"
  "gcc-c++"
  "git"
  "glib2-devel"
  "glibc-devel"
  "glibc-static"
  # "go"
  "golang-github-cpuguy83-md2man"
  "gpgme-devel"
  "helm"
  "iproute"
  "iptables"
  "jq"
  "libassuan-devel"
  "libffi-devel"
  "libgpg-error-devel"
  "libseccomp-devel"
  "libselinux-devel"
  "libtool"
  "llvm-toolset"
  "make"
  "mariadb-devel"
  "nano"
  "neovim"
  "ninja-build"
  "openldap-devel"
  "openssl-devel"
  "pkgconfig"
  "pkgconf-pkg-config"
  "python-devel"
  "python3-devel"
  "readline-devel"
  "rpm-build"
  "socat"
  "sqlite"
  "sqlite-devel"
  "tcpdump"
  "tk-devel"
  "tmux"
  "vim"
  "wget"
  "xz-devel"
  "yum-utils"
  "zlib-devel"
  "zsh"
)

for pkg in "${rpm_deps[@]}"; do
  dnf install -y "$pkg" || { echo -e "\n\n[ERROR] Failed to install ${pkg}"; sleep 3; }
done
