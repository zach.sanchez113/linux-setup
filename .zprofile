# Load in rustup
[ -f "${HOME}/.cargo/env" ] && . "${HOME}/.cargo/env"

# Set up pyenv
if [[ -d "$HOME/.pyenv" ]]; then
  export PYENV_ROOT="$HOME/.pyenv"
  export PATH="$PYENV_ROOT/bin:$PATH"
  eval "$(pyenv init --path)"
fi

# Set up choosenim
export PATH="$HOME/.nimble/bin:$PATH"

# Set up RVM correctly
export PATH="$PATH:$HOME/.rvm/bin"

# Load RVM into a shell session *as a function*
[[ -s "$HOME/.rvm/scripts/rvm" ]] && source "$HOME/.rvm/scripts/rvm"

# Sometimes tab completion glitches out, and this fixes it for $reasons
export LANG=en_US.UTF-8
export LC_ALL=en_US.UTF-8
