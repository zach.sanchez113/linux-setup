#!/usr/bin/env bash

##################################################################################################
#                                                                                                #
#    Install various utilities, i.e. language version managers, rust utils, golang utils,        #
#    anything that doesn't make sense with the package manager                                   #
#                                                                                                #
##################################################################################################


# Bash strict mode
# See: http://redsymbol.net/articles/unofficial-bash-strict-mode/
set -e
set -u
set -o pipefail
shopt -s failglob


# Check if a command exists
#
function check_cmd() {
  command -v "$1" > /dev/null 2>&1
}

# Install zsh
# - Get source tarball: https://zsh.sourceforge.io/Arc/source.html
# - ./configure
# - make
# - make check
# - make install
# - command -v zsh | sudo tee -a /etc/shells
#
function install_zsh() {

  echo -e "\n[$(date)] Couldn't find zsh, compiling from source..."
  sleep 1

  curl --location --silent --show-error https://sourceforge.net/projects/zsh/files/latest/download | sudo tar -xJf - -C /usr/local/src/

  zsh_src_dir=$(find /usr/local/src -name "zsh*" -type d 2>/dev/null)

  pushd "${zsh_src_dir}"
  sudo ./configure
  popd

  sudo make -C "${zsh_src_dir}" \
    || { echo -e "\nFailed to run 'make' - check the output and resolve.\n"; exit 1; }

  sudo make check -C "${zsh_src_dir}" \
    || { echo -e "\nFailed to run 'make check' - manually inspect test output and run 'make install' if everything looks OK.\n"; exit 1; }

  sudo make install -C "${zsh_src_dir}" \
    || { echo -e "\nFailed to run 'make install' - check the output and resolve.\n"; exit 1; }

  command -v zsh 2>/dev/null | sudo tee -a /etc/shells

  echo -e "\n[$(date)] Done installing zsh!"

}

# Golang utilities I enjoy using
#
function install_golang_utilties() {

  export GOPATH="${HOME}/go"
  export PATH="${PATH}:/usr/local/go/bin"
  export PATH="${HOME}/go/bin:${PATH}"

  go install mvdan.cc/sh/v3/cmd/shfmt@latest

  # vgrep (pager for rg, git grep, and grep)
  sudo mkdir -p /usr/local/src/vgrep
  sudo chmod 777 /usr/local/src/vgrep
  git clone https://github.com/vrothberg/vgrep.git /usr/local/src/vgrep

  pushd /usr/local/src/vgrep
  sudo PATH="${PATH}:/usr/local/go/bin" make build
  sudo PATH="${PATH}:/usr/local/go/bin" make install
  popd

  # duf (alternative to df)
  wget -P /usr/local/src/ \
     https://github.com/muesli/duf/releases/download/v0.8.1/duf_0.8.1_linux_x86_64.tar.gz

  pushd /usr/local/src/
  tar xvf duf_0.8.1_linux_x86_64.tar.gz
  popd

  sudo cp /usr/local/src/duf/duf /usr/local/bin/duf

}

# Install rustup (requires internet connectivity - install rust/cargo via package manager otherwise)
#
function install_rustup() {

  echo -e "\n[$(date)] Installing rustup..."

  curl --proto '=https' --tlsv1.2 -sSf https://sh.rustup.rs | sh
  source "${HOME}/.cargo/env"

  # TODO: Complaints about missing Cargo.toml?
  # echo "Running checks for 'cargo':"
  # cargo check 1>/dev/null || echo -e "\nLooks like you're missing some build dependencies! Don't forget to install them."

}

# Rust utilities I enjoy using
#
function install_rust_utilities() {

  echo -e "\n[$(date)] Installing rust utilities, please hold...\n"
  sleep 1

  [ -f "${HOME}/.cargo/env" ] && . "${HOME}/.cargo/env"

  # Minimal set of crates that I actually use regularly
  # For my own reference when spinning up VMs
  declare -a crates=(
    "bat"               # 'cat', but with paging and colorful syntax highlighting
    "btm"               # 'top' replacement that's actually usable (project: 'bottom')
    "cross"             # Easily cross-compile Rust-based projects via containers
    "du-dust"           # 'du' replacement to better visualize disk usage (project: 'dust')
    "dua-cli"           # View disk space usage and delete unwanted data, fast.
    "exa"               # Simple and pretty 'ls' replacement
    "procs"             # The way 'ps' probably should've been implemented
    "rustcat"           # or 'rcat', is a smarter netcat (has command history/completion, ctrl+c blocking)
    "ripgrep"           # Better grep, especially for git repos (cmd: 'rg')
    "sd"                # 'sed' replacement
    "tealdeer"          # Example-based "manpage" (cmd: 'tldr')
    "zellij"            # 'tmux' replacement
  )

  # Full install
  declare -a crates=(
    "bat"               # 'cat', but with paging and colorful syntax highlighting
    "broot"             # Better FS navigation + 'cd' replacement
    "btm"               # 'top' replacement that's actually usable (project: 'bottom')
    "cross"             # Easily cross-compile Rust-based projects via containers
    "delicate"          # cronjob replacement (sorta) with a frontend
    "du-dust"           # 'du' replacement to better visualize disk usage (project: 'dust')
    "dua-cli"           # View disk space usage and delete unwanted data, fast.
    "exa"               # Simple and pretty 'ls' replacement
    "fblog"             # JSON log viewer
    "funzzy"            # Execute commands on file change (better for persistent/granular setups)
    "gfold"             # Monitor multiple git repos
    "mdbook"            # Markdown-based docs in a book format
    "mdbook-theme"      # Adds extra bells and whistles to mdbook
    "procs"             # The way 'ps' probably should've been implemented
    "pueue"             # Task queue for shell commands
    "rargs"             # xargs + awk helper (?)
    "rustcat"           # or 'rcat', is a smarter netcat (has command history/completion, ctrl+c blocking)
    "ripgrep"           # Better grep, especially for git repos (cmd: 'rg')
    "sd"                # 'sed' replacement
    "systeroid"         # 'sysctl' replacement
    "tealdeer"          # Example-based "manpage" (cmd: 'tldr')
    "tokei"             # Code metrics
    "watchexec"         # Execute commands on file change (better for one-off)
    "xh"                # 'curl' replacement
    "zellij"            # 'tmux' replacement
  )

  for crate in "${crates[@]}"; do
    cargo install "${crate}"
    # cargo install --locked "${crate}"
  done

  # Copy the config file for btm
  if [ -f ./templates/bottom.toml ]; then
    mkdir -p "${HOME}/.config/bottom/"
    cp -v ./templates/bottom.toml "${HOME}/.config/bottom/"
  else
    echo -e "\n[$(date)] Failed to locate bottom.toml!"
  fi

  # bat-extras
  sudo mkdir -p /usr/local/src/bat-extras
  sudo chmod 777 /usr/local/src/bat-extras
  git clone https://github.com/eth-p/bat-extras.git /usr/local/src/bat-extras

  pushd /usr/local/src/bat-extras

  ./build.sh
  cp ./bin/* "${HOME}/.cargo/bin/"

  popd

  # Install 'dog' DNS client
  # There isn't a crate for 'dog', so it's gotta be compiled manually
  # TODO: manpath
  sudo mkdir -p /usr/local/src/dog
  sudo chmod 777 /usr/local/src/dog
  git clone https://github.com/ogham/dog.git /usr/local/src/dog

  pushd /usr/local/src/dog

  cargo build --release
  cargo test

  # TODO: Manpage doesn't work (need to configure manpath IIRC)
  cp target/release/dog "${HOME}/.local/bin/dog"
  cp ./man/dog.1.md "${ZSH}/man/man1/dog.1.md"

  popd

  echo -e "\n[$(date)] Done installing rust utlities!"

}

# Install gum for fabulous shell scripts
#
function install_gum() {

  pushd /tmp/

  wget https://github.com/charmbracelet/gum/releases/download/v0.8.0/gum_0.8.0_linux_x86_64.tar.gz
  tar xvf ./gum_0.8.0_linux_x86_64.tar.gz
  sudo mv gum /usr/local/bin/
  sudo chown root:root /usr/local/bin/gum
  sudo chmod 755 /usr/local/bin/gum

  popd

}

function main() {

  # Install zsh first if needed
  if ! check_cmd zsh; then
    install_zsh
  fi

  # Install rustup before ZSH framework since it'll append to dotfiles
  if ! check_cmd rustup; then
    echo -e "\n[$(date)] Installing rustup..."
    install_rustup
    source "${HOME}/.cargo/env"
  fi

  # oh-my-zsh + zsh dotfiles
  echo -e "\n[$(date)] Installing oh-my-zsh..."
  sh -c "$(curl -fsSL https://raw.github.com/ohmyzsh/ohmyzsh/master/tools/install.sh)"
  cp "$(dirname "${0}")/.zshrc" "${HOME}/.zshrc"
  cp "$(dirname "${0}")/.zshenv" "${HOME}/.zshenv"
  cp "$(dirname "${0}")/.oh-my-zsh-theme" "${HOME}/.oh-my-zsh/custom/themes/z-duellj.zsh-theme"

  # Add zsh-syntax-highlighting as an OMZ plugin
  git clone https://github.com/zsh-users/zsh-syntax-highlighting.git ~/.oh-my-zsh/custom/plugins/zsh-syntax-highlighting

  # pyenv
  # Prerequisites: https://github.com/pyenv/pyenv/wiki#suggested-build-environment
  echo -e "\n[$(date)] Installing pyenv..."
  curl -L https://github.com/pyenv/pyenv-installer/raw/master/bin/pyenv-installer | zsh

  # nvm
  echo -e "\n[$(date)] Installing nvm..."
  curl -o- https://raw.githubusercontent.com/nvm-sh/nvm/v0.39.1/install.sh | zsh

  # golang
  echo -e "\n[$(date)] Installing Go..."
  curl --location --silent --show-error https://go.dev/dl/go1.22.3.linux-amd64.tar.gz | sudo tar -xzf - -C /usr/local/
  mkdir -p "${HOME}/go/bin" "${HOME}/go/pkg" "${HOME}/go/src"

  # golang utilties
  install_golang_utilties

  # Install gvm (requires version for bootstrapping)
  echo -e "\n[$(date)] Installing gvm..."
  zsh < <(curl -s -S -L https://raw.githubusercontent.com/moovweb/gvm/master/binscripts/gvm-installer)

  # Install rvm
  # NOTE: For FW issues, you can download keys from the web (https://rvm.io/rvm/security#alternatives)
  declare -a gpg_keyservers=(
    "hkp://pool.sks-keyservers.net"
    "hkp://ipv4.pool.sks-keyservers.net"
    "hkp://pgp.mit.edu"
    "hkp://keyserver.pgp.com"
  )

  for gpg_keyserver in "${gpg_keyservers[@]}"; do

    if gpg --keyserver "$gpg_keyserver" --recv-keys 409B6B1796C275462A1703113804BB82D39DC0E3 7D2BAF1CF37B13E2069D6956105BD0E739499BDB; then

      # NOTE: The script uses shopt, which isn't available in ZSH
      curl -sSL https://get.rvm.io | bash -s stable --ruby
      break

    fi

  done

  # choosenim
  curl https://nim-lang.org/choosenim/init.sh -sSf | sh

  # git-extras
  # TODO: RHEL only?
  echo -e "\n[$(date)] Installing git-extras..."
  curl --location --silent --show-error https://git.io/git-extras-setup | sudo zsh /dev/stdin

  # hjson
  curl -sSL https://github.com/hjson/hjson-go/releases/download/v4.2.0/hjson_v4.2.0_linux_amd64.tar.gz | sudo tar -xz -C /usr/local/bin

  # yj
  sudo wget https://github.com/sclevine/yj/releases/download/v5.1.0/yj-linux-amd64 -O /usr/local/bin/yj
  sudo chmod a+x /usr/local/bin/yj

  # Install rust utilties last since they will take a while to install
  install_rust_utilities

  echo -e "\n[$(date)] Done!"

}


# ! Disabled so that this can be run piecemeal
# main
