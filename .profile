# Load in rustup
[ -f "${HOME}/.cargo/env" ] && . "${HOME}/.cargo/env"

# Set up pyenv
if [[ -d "$HOME/.pyenv" ]]; then
  export PYENV_ROOT="$HOME/.pyenv"
  export PATH="$PYENV_ROOT/bin:$PATH"
  eval "$(pyenv init --path)"
fi

# Set up choosenim
export PATH="$HOME/.nimble/bin:$PATH"
