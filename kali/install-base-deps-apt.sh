#!/usr/bin/env bash

##################################################################################################
#                                                                                                #
#    Install various dependencies via apt, i.e. various utilities, libraries, etc.,              #
#    but with a focus on Kali Linux because:                                                     #
#                                                                                                #
#    a) I don't want every little thing on Kali,                                                 #
#    b) the base installation includes some tools already (e.g. PowerShell), and                 #
#    c) there are some Kali-specific packages that aren't present in base Debian                 #
#                                                                                                #
#    So really, this is just so I don't muddy the waters whenever I need to (re)install.         #
#                                                                                                #
##################################################################################################


# Bash strict mode
# See: http://redsymbol.net/articles/unofficial-bash-strict-mode/
set -e
set -u
set -o pipefail
shopt -s failglob

# Because there's a lot of sudo here and I'm being lazy
if [ $EUID -ne 0 ]; then
  echo "This script must be run as root" >&2
  exit 1
fi

# To store .deb, scripts, etc.
if ! [ -d /usr/local/pkg ]; then
  mkdir -p /usr/local/pkg
  chown zsanchez113:zsanchez113 /usr/local/pkg
fi


#################################################
#
# Upgrade everything first
#
#################################################

apt update
apt dist-upgrade -y

# We'll need this soon
apt install -y apt-transport-https ca-certificates curl wget


#################################################
#
# Add extra repos
#
#################################################

# PowerShell, .NET, etc.
# ! Skip this for Kali since it's already installed by default
# wget https://packages.microsoft.com/config/ubuntu/20.04/packages-microsoft-prod.deb -O /usr/local/pkg/packages-microsoft-prod.deb
# apt install -y /usr/local/pkg/packages-microsoft-prod.deb

# Nala
# ! WARNING: Doesn't necessarily work 100% with Kali
echo "deb https://deb.volian.org/volian/ scar main" | sudo tee /etc/apt/sources.list.d/volian-archive-scar-unstable.list
wget -qO - https://deb.volian.org/volian/scar.key | sudo tee /etc/apt/trusted.gpg.d/volian-archive-scar-unstable.gpg > /dev/null

# Brave Browser
#
# Settings:
#
# - Show bookmarks
# - Enable autocomplete, switch to Google search
# - Enable full address + long address bar
# - Switch to GTK+ and use system title + borders
# - Don't show Brave Rewards
#
# TODO: Proxy setup for Burp/ZAP
#
curl -fsSLo /usr/share/keyrings/brave-browser-archive-keyring.gpg https://brave-browser-apt-release.s3.brave.com/brave-browser-archive-keyring.gpg
echo "deb [signed-by=/usr/share/keyrings/brave-browser-archive-keyring.gpg arch=amd64] https://brave-browser-apt-release.s3.brave.com/ stable main" | tee /etc/apt/sources.list.d/brave-browser-release.list

# Kubernetes
curl -fsSLo /usr/share/keyrings/kubernetes-archive-keyring.gpg https://packages.cloud.google.com/apt/doc/apt-key.gpg
echo "deb [signed-by=/usr/share/keyrings/kubernetes-archive-keyring.gpg] https://apt.kubernetes.io/ kubernetes-xenial main" | tee /etc/apt/sources.list.d/kubernetes.list

# OWASP ZAP repo
echo 'deb http://download.opensuse.org/repositories/home:/cabelo/xUbuntu_20.04/ /' | tee /etc/apt/sources.list.d/home:cabelo.list
curl -fsSL https://download.opensuse.org/repositories/home:cabelo/xUbuntu_20.04/Release.key \
  | gpg --dearmor \
  | tee /etc/apt/trusted.gpg.d/home_cabelo.gpg > /dev/null


#################################################
#
# Install desired packages
#
#################################################

apt update

declare -a apt_deps=(
  "apt-file"
  "autoconf"
  "automake"
  "autopoint"
  "autotools-dev"
  "brave-browser"
  "btrfs-progs"
  "build-essential"
  "byacc"
  "chisel"
  "clang-format"
  "cmake"
  "curl"
  "debhelper"
  "dirsearch"
  # There isn't really any point to this on a Kali VM
  # "dkms"
  "docker"
  "docker-compose"
  # SDK version may change
  # "dotnet-sdk-6.0"
  "fakeroot"
  "feroxbuster"
  "flex"
  "gengetopt"
  "gettext"
  "git"
  "git-extras"
  "go-md2man"
  "iptables"
  "joomscan"
  "jq"
  "kubectl"
  # "language-support-*"  # not found
  "ldap-utils"
  "libassuan-dev"
  "libbtrfs-dev"
  "libbz2-dev"
  "libc6-dev"
  "libdevmapper-dev"
  "libffi-dev"
  "libglib2.0-dev"
  "libgmp3-dev"
  "libgpg-error-dev"
  "libgpgme-dev"
  "libjson-c-dev"
  "liblzma-dev"
  "libncursesw5-dev"
  "libpcap-dev"
  "libprotobuf-c-dev"
  "libprotobuf-dev"
  "libreadline-dev"
  "libseccomp-dev"
  "libselinux1-dev"
  "libsqlite3-dev"
  "libssl-dev"
  "libsystemd-dev"
  "libtool"
  "libunistring-dev"
  "libxml2-dev"
  "libxmlsec1-dev"
  "llvm"
  "make"
  # "nala"  # only for Ubuntu 22.04+
  # "nala-legacy"  # nala-legacy : Depends: libpython3.9 but it is not installable
  "nikto"
  "nmap"
  "nodejs"
  "npm"
  "odat"
  "oscanner"
  "owasp-zap"
  # "python-dev"  # not found
  "python-setuptools"
  "python3-dev"
  "python3-impacket"
  "python3-ldap3"
  "python3-yaml"
  "python3-setuptools"
  "pkg-config"
  "podman"
  "powershell"
  "redis-tools"
  "runc"
  "s3scanner"
  "seclists"
  "sipvicious"
  "smbclient"
  "smtp-user-enum"
  "sslscan"
  "svwar"
  "tk-dev"
  "tnscmd10g"
  "uidmap"
  "wget"
  "wireshark"
  "wkhtmltopdf"
  "wpscan"
  "xz-utils"
  "yarnpkg"
  "zlib1g-dev"
  "zsh"
)

# If something's missing, this script starts having some issues...
for pkg in "${apt_deps[@]}"; do
  apt install -y "$pkg" || { echo -e "\n\n[ERROR] Failed to install ${pkg}"; sleep 3; }
done

# But, if you're feeling confident (won't hurt)...
# apt install "${apt_deps[@]}"
