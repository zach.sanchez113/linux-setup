# Kali-specific files

## /etc/profile

oh-my-zsh won't work correctly with the default `/etc/zprofile`. ZSH load order breaks during `sudo`, where `.zshenv` is loaded, but then `PATH` is completely overridden instead of munged.

## init.sh

So I can easily reproduce my setup (gotta love reinstalls...)
