#!/usr/bin/env bash

##################################################################################################
#                                                                                                #
#      Install various tools that are more geared towards Kali and aren't available via apt      #
#                                                                                                #
##################################################################################################

# TODO: yaptest (has prereqs): https://pentestmonkey.net/yaptest/installing/yaptest-installation
# TODO: nmapAutomator


##################################################
#
# Install AutoRecon
#
##################################################

# region autorecon

pip install --user pipx
pipx install git+https://github.com/Tib3rius/AutoRecon.git

# endregion


##################################################
#
# Install linWinPwn
#
##################################################

# region linWinPwn

pushd /usr/local/src || { echo 'Bruh...'; exit 1; }
git clone https://github.com/lefayjey/linWinPwn.git

pushd linWinPwn || { echo 'Bruh...'; exit 1; }
chmod +x install.sh
install.sh

chmod +x linWinPwn.sh

sudo ln -s /usr/local/src/linWinPwn/linWinPwn.sh /usr/local/bin/linWinPwn
popd || { echo 'Bruh...'; exit 1; }
popd || { echo 'Bruh...'; exit 1; }

# endregion


##################################################
#
# Install enum4linux-ng
#
##################################################

# region enum4linux-ng

pushd /usr/local/src || { echo 'Bruh...'; exit 1; }
git clone https://github.com/cddmp/enum4linux-ng.git

pushd enum4linux-ng || { echo 'Bruh...'; exit 1; }
sudo ln -s /usr/local/src/enum4linux-ng/enum4linux-ng.py /usr/local/bin/enum4linux-ng

popd || { echo 'Bruh...'; exit 1; }
popd || { echo 'Bruh...'; exit 1; }

# endregion


##################################################
#
# Install droopescan
#
##################################################

# region droopescan

pushd /usr/local/src || { echo 'Bruh...'; exit 1; }
git clone https://github.com/droope/droopescan.git

pushd droopescan || { echo 'Bruh...'; exit 1; }
pip install -r requirements.txt
sudo ln -s /usr/local/src/droopescan/droopescan /usr/local/bin/droopescan

popd || { echo 'Bruh...'; exit 1; }
popd || { echo 'Bruh...'; exit 1; }

# endregion


##################################################
#
# Install zgrab2
#
##################################################

# region zgrab2

pushd /usr/local/src || { echo 'Bruh...'; exit 1; }
git clone https://github.com/zmap/zgrab2.git
popd || { echo 'Bruh...'; exit 1; }

pushd /usr/local/src/zgrab2/cmd/zgrab2 || { echo 'Bruh...'; exit 1; }
go build
sudo ln -s /usr/local/src/zgrab2/cmd/zgrab2/zgrab2 /usr/local/bin/zgrab2
popd || { echo 'Bruh...'; exit 1; }

# endregion


##################################################
#
# Install lfimap
#
##################################################

# region lfimap

pushd /usr/local/src || { echo 'Bruh...'; exit 1; }
git clone https://github.com/hansmach1ne/lfimap.git

pushd lfimap || { echo 'Bruh...'; exit 1; }
pip install -r requirements.txt
sudo ln -s /usr/local/src/lfimap/lfimap.py /usr/local/bin/lfimap

popd || { echo 'Bruh...'; exit 1; }
popd || { echo 'Bruh...'; exit 1; }

# endregion


##################################################
#
# Install ysoserial
#
##################################################

# region ysoserial

wget -O /usr/local/pkg/ysoserial.jar \
  https://github.com/frohoff/ysoserial/releases/latest/download/ysoserial-all.jar

echo "#!/usr/bin/env bash" | sudo tee /usr/local/bin/ysoserial
echo "java -jar /usr/local/pkg/ysoserial.jar" | sudo tee -a /usr/local/bin/ysoserial

sudo chmod +x /usr/local/bin/ysoserial

# endregion


##################################################
#
# Install NetExec
#
##################################################

pipx ensurepath
pipx install git+https://github.com/Pennyw0rth/NetExec


##################################################
#
# Install wappalyzer
#
# TODO: This doesn't work anymore! Need to create a private version
#
##################################################

# region wappalyzer

# TODO: assumes nvm
# TODO: installed nodejs -> xfce outdated binary
# pushd /usr/local/src || { echo 'Bruh...'; exit 1; }
# sudo git clone https://github.com/wappalyzer/wappalyzer.git
# sudo chown -R kali:kali wappalyzer
# pushd wappalyzer || { echo 'Bruh...'; exit 1; }
# yarn install
# yarn run link
# sudo ln -s $(readlink -f ./src/drivers/npm/cli.js) /usr/local/bin/wappalyzer
# popd || { echo 'Bruh...'; exit 1; }
# popd || { echo 'Bruh...'; exit 1; }

# endregion
