#!/usr/bin/env bash

##################################################################################################
#                                                                                                #
#           Set up HTTP + SMB servers with tools to use on compromised hosts                     #
#                                                                                                #
##################################################################################################

mkdir /home/kali/git

sudo mkdir -p /srv/{http,smb,webdav} /srv/http/{windows,linux}/x86 /usr/local/pkg/
sudo chown -R kali: /srv/*
sudo chown -R kali: /usr/local/pkg/

sudo touch /etc/systemd/system/{selfhosted-80,selfhosted-8000,smbserver}.service
sudo chown kali: /etc/systemd/system/{selfhosted-80,selfhosted-8000,smbserver}.service

wget -O /etc/systemd/system/selfhosted-80.service \
   https://gitlab.com/zach.sanchez113/security-experiments/-/raw/master/linux/selfhosted-80.service?ref_type=heads

wget -O /etc/systemd/system/selfhosted-8000.service \
   https://gitlab.com/zach.sanchez113/security-experiments/-/raw/master/linux/selfhosted-8000.service?ref_type=heads

wget -O /etc/systemd/system/smbserver.service \
   https://gitlab.com/zach.sanchez113/security-experiments/-/raw/master/linux/smbserver.service?ref_type=heads

# Enable + start everything
sudo systemctl daemon-reload
sudo systemctl enable --now selfhosted-80.service
sudo systemctl enable --now selfhosted-8000.service
sudo systemctl enable --now smbserver.service


##################################################
#
# Download generic tools for serving over HTTP
#
##################################################

# region generic tools

# Wordpress webshell as a plugin
wget -O /srv/http/wordpress-webshell-plugin-1.1.0.zip \
  https://github.com/p0dalirius/Wordpress-webshell-plugin/archive/refs/tags/1.1.0.zip

# Webshell
wget -O /usr/share/webshells/php/wwwolf-php-webshell.php \
  https://raw.githubusercontent.com/WhiteWinterWolf/wwwolf-php-webshell/master/webshell.php
wget -O -P /srv/http/wwwolf-php-webshell.php \
  https://raw.githubusercontent.com/WhiteWinterWolf/wwwolf-php-webshell/master/webshell.php

# Chisel
wget -P /usr/local/pkg/ \
  https://github.com/jpillora/chisel/releases/download/v1.9.1/chisel_1.9.1_linux_amd64.gz

wget -P /usr/local/pkg/ \
  https://github.com/jpillora/chisel/releases/download/v1.9.1/chisel_1.9.1_windows_amd64.gz

pushd /usr/local/pkg/ || { echo 'Bruh...'; exit 1; }

# Decompress the Linux binary
gunzip ./chisel_1.9.1_linux_amd64.gz

# Use both filenames just in case
cp ./chisel_1.9.1_linux_amd64 /srv/http/linux/chisel
cp ./chisel_1.9.1_linux_amd64 /srv/http/linux/chisel_1.9.1_linux_amd64

# Put this in /usr/local/bin/ so we can spin up a server
sudo cp ./chisel_1.9.1_linux_amd64 /usr/local/bin/chisel
sudo chmod a+x /usr/local/bin/chisel

# Decompress the Windows binary
gunzip ./chisel_1.9.1_windows_amd64.gz

# Use both filenames just in case
cp ./chisel_1.9.1_windows_amd64 /srv/http/windows/chisel
cp ./chisel_1.9.1_windows_amd64 /srv/http/windows/chisel_1.9.1_windows_amd64

popd || { echo 'Bruh...'; exit 1; }

# endregion
