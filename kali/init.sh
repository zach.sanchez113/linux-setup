#!/usr/bin/env bash

##################################################################################################
#                                                                                                #
#                    Perform basic initialization of a new Kali machine                          #
#                                                                                                #
#                       See the other scripts for further setup                                  #
#                                                                                                #
##################################################################################################

##################################################
#
# Basics
#
##################################################

# region basics

sudo apt update
sudo apt upgrade

# Use this with reverse shell handlers!
sudo apt install rlwrap

# RDP client
sudo apt install remmina

# Required by AutoRecon
sudo apt install seclists curl dnsrecon enum4linux feroxbuster gobuster impacket-scripts nbtscan nikto nmap onesixtyone oscanner redis-tools smbclient smbmap snmp sslscan sipvicious tnscmd10g whatweb wkhtmltopdf

# Useful for holding source code for utilities we'll be installing later
sudo mkdir -p /usr/local/src
sudo chown kali:kali /usr/local/src
sudo chmod 777 /usr/local/src

# endregion


##################################################
#
# For RDP in AWS
#
##################################################

# region RDP

sudo apt install kali-desktop-xfce xrdp

sudo systemctl daemon-reload
sudo systemctl enable xrdp
sudo systemctl start xrdp

# endregion
