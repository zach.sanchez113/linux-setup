#!/usr/bin/env bash

# Bash strict mode
# See: http://redsymbol.net/articles/unofficial-bash-strict-mode/
set -e
set -u
set -o pipefail
shopt -s failglob

# Because there's a lot of sudo here and I'm being lazy
if [ $EUID -ne 0 ]; then
  echo "This script must be run as root" >&2
  exit 1
fi

# To store .deb, scripts, etc.
if ! [ -d /usr/local/pkg ]; then
  mkdir -p /usr/local/pkg
  chown zsanchez113:zsanchez113 /usr/local/pkg
fi


#################################################
#
# Upgrade everything first
#
#################################################

apt update
apt dist-upgrade -y

# We'll need this soon
apt install -y apt-transport-https ca-certificates curl wget gpg


#################################################
#
# Add extra repos
#
# TODO: Slack, ProtonVPN
#
#################################################

# AppImageLauncher repo
# Makes life a bit easier when it comes to AppImage
# TODO: Need apt dependency 'software-properties-common'
# TODO: This repo is missing a Release file, will cause `apt-update` to halt
add-apt-repository ppa:appimagelauncher-team/stable

# OWASP ZAP repo
echo 'deb http://download.opensuse.org/repositories/home:/cabelo/xUbuntu_22.04/ /' | tee /etc/apt/sources.list.d/home:cabelo.list
curl -fsSL https://download.opensuse.org/repositories/home:cabelo/xUbuntu_22.04/Release.key \
  | gpg --dearmor \
  | tee /etc/apt/trusted.gpg.d/home_cabelo.gpg > /dev/null

# 1Password
curl -sS https://downloads.1password.com/linux/keys/1password.asc | gpg --dearmor --output /usr/share/keyrings/1password-archive-keyring.gpg
echo 'deb [arch=amd64 signed-by=/usr/share/keyrings/1password-archive-keyring.gpg] https://downloads.1password.com/linux/debian/amd64 stable main' \
  | tee /etc/apt/sources.list.d/1password.list

mkdir -p /etc/debsig/policies/AC2D62742012EA22/
curl -sS https://downloads.1password.com/linux/debian/debsig/1password.pol | tee /etc/debsig/policies/AC2D62742012EA22/1password.pol
mkdir -p /usr/share/debsig/keyrings/AC2D62742012EA22
curl -sS https://downloads.1password.com/linux/keys/1password.asc | gpg --dearmor --output /usr/share/debsig/keyrings/AC2D62742012EA22/debsig.gpg

# Brave Browser
#
# Settings:
#
# - Show bookmarks
# - Enable autocomplete, switch to Google search
# - Enable full address + long address bar
# - Switch to GTK+ and use system title + borders
# - Don't show Brave Rewards
#
curl -fsSLo /usr/share/keyrings/brave-browser-archive-keyring.gpg https://brave-browser-apt-release.s3.brave.com/brave-browser-archive-keyring.gpg
echo "deb [signed-by=/usr/share/keyrings/brave-browser-archive-keyring.gpg arch=amd64] https://brave-browser-apt-release.s3.brave.com/ stable main" | tee /etc/apt/sources.list.d/brave-browser-release.list

# Signal
wget -O- https://updates.signal.org/desktop/apt/keys.asc | gpg --dearmor > /usr/share/keyrings/signal-desktop-keyring.gpg

# C'mon now...
# cat signal-desktop-keyring.gpg | sudo tee -a /usr/share/keyrings/signal-desktop-keyring.gpg > /dev/null

echo 'deb [arch=amd64 signed-by=/usr/share/keyrings/signal-desktop-keyring.gpg] https://updates.signal.org/desktop/apt xenial main' | tee -a /etc/apt/sources.list.d/signal-xenial.list


# VS Code
wget -qO- https://packages.microsoft.com/keys/microsoft.asc | gpg --dearmor > packages.microsoft.gpg
sudo install -D -o root -g root -m 644 packages.microsoft.gpg /etc/apt/keyrings/packages.microsoft.gpg
sudo sh -c 'echo "deb [arch=amd64,arm64,armhf signed-by=/etc/apt/keyrings/packages.microsoft.gpg] https://packages.microsoft.com/repos/code stable main" > /etc/apt/sources.list.d/vscode.list'
rm -f packages.microsoft.gpg


#################################################
#
# Install desired packages
#
#################################################

apt update

declare -a apt_deps=(
  "1password"
  "appimagelauncher"
  "brave-browser"
  "code"
  "owasp-zap"
  "signal-desktop"
  "vagrant"
  "vlc"
  "wireshark"
)

# If something's missing, this script starts having some issues...
for pkg in "${apt_deps[@]}"; do
  apt install -y "$pkg" || { echo -e "\n\n[ERROR] Failed to install ${pkg}"; sleep 3; }
done

# But, if you're feeling confident (won't hurt)...
# apt install -y "${apt_deps[@]}"


###########################################################################
#
# Nvidia drivers
#
# Don't forget to account for the OS + release!
# Select OS -> release -> network below, then adjust the URL/steps based on output:
# https://developer.nvidia.com/cuda-downloads?target_os=Linux&target_arch=x86_64
#
# By the time this is needed, it'll almost certainly be out of date.
# Double-check for the latest versions.
#
###########################################################################

# For the CUDA toolkit
wget https://developer.download.nvidia.com/compute/cuda/repos/ubuntu2204/x86_64/cuda-keyring_1.0-1_all.deb
sudo dpkg -i cuda-keyring_1.0-1_all.deb
sudo apt update

# Packages for Ubuntu 22.04 as of writing
# TODO: Is there a way to install drivers without the version number?
sudo apt -y install \
  cuda \
  dkms \
  nvidia-driver-545


# So that I don't forget the steps...
#
function uninstall_nvidia_drivers() {

  sudo apt update
  sudo apt upgrade
  sudo apt autoremove
  sudo apt remove --purge nvidia*
  sudo apt remove --purge "nvidia*"
  sudo reboot now

}

#################################################
#
# Extra packages from external locations
#
#################################################

# VirtualBox
wget "https://download.virtualbox.org/virtualbox/6.1.30/virtualbox-6.1_6.1.30-148432~Ubuntu~eoan_amd64.deb"  -P /usr/local/pkg/
apt install -y /usr/local/pkg/virtualbox-6.1_6.1.30-148432~Ubuntu~eoan_amd64.deb

# Discord
# ! Add this to ~/.config/discord/settings.json: "SKIP_HOST_UPDATE": true
wget "https://discord.com/api/download?platform=linux&format=deb" -O /usr/local/pkg/discord.deb
apt install -y /usr/local/pkg/discord.deb

# OnlyOffice
wget "https://download.onlyoffice.com/install/desktop/editors/linux/onlyoffice-desktopeditors_amd64.deb" -P /usr/local/pkg/
apt install -y /usr/local/pkg/onlyoffice-desktopeditors_amd64.deb

# Burp Suite
# TODO: Will this work right down the road?
wget "https://portswigger-cdn.net/burp/releases/download?product=community&version=2022.5.2&type=Linux" -O /usr/local/pkg/install-burp-2022.5.2.sh
bash /usr/local/pkg/install-burp-2022.5.2.sh

# Obsidian
# ! This will require some manual work
wget "https://github.com/obsidianmd/obsidian-releases/releases/download/v0.14.15/Obsidian-0.14.15.AppImage" -P /usr/local/pkg/
AppImageLauncher /usr/local/pkg/Obsidian-0.14.15.AppImage
