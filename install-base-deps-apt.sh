#!/usr/bin/env bash

##################################################################################################
#                                                                                                #
#    Install various dependencies via apt, i.e. various utilities, libraries, etc.               #
#                                                                                                #
##################################################################################################


# Bash strict mode
# See: http://redsymbol.net/articles/unofficial-bash-strict-mode/
set -e
set -u
set -o pipefail
shopt -s failglob

# Because there's a lot of sudo here and I'm being lazy
if [ $EUID -ne 0 ]; then
  echo "This script must be run as root" >&2
  exit 1
fi

# To store .deb, scripts, etc.
if ! [ -d /usr/local/pkg ]; then
  mkdir -p /usr/local/pkg
  chown zsanchez113:zsanchez113 /usr/local/pkg
fi


#################################################
#
# Upgrade everything first
#
#################################################

apt update
apt dist-upgrade -y

# We'll need this soon
apt install -y apt-transport-https ca-certificates curl wget


#################################################
#
# Add extra repos
#
#################################################

# PowerShell, .NET, etc.
wget https://packages.microsoft.com/config/ubuntu/22.04/packages-microsoft-prod.deb -O /usr/local/pkg/packages-microsoft-prod.deb
apt install -y /usr/local/pkg/packages-microsoft-prod.deb

# Kubernetes
curl -fsSLo /usr/share/keyrings/kubernetes-archive-keyring.gpg https://packages.cloud.google.com/apt/doc/apt-key.gpg
echo "deb [signed-by=/usr/share/keyrings/kubernetes-archive-keyring.gpg] https://apt.kubernetes.io/ kubernetes-xenial main" | tee /etc/apt/sources.list.d/kubernetes.list

# Helm
curl https://baltocdn.com/helm/signing.asc | gpg --dearmor | sudo tee /usr/share/keyrings/helm.gpg > /dev/null
sudo echo "deb [arch=$(dpkg --print-architecture) signed-by=/usr/share/keyrings/helm.gpg] https://baltocdn.com/helm/stable/debian/ all main" > /etc/apt/sources.list.d/helm-stable-debian.list

# Nala
echo "deb https://deb.volian.org/volian/ scar main" | sudo tee /etc/apt/sources.list.d/volian-archive-scar-unstable.list
wget -qO - https://deb.volian.org/volian/scar.key | sudo tee /etc/apt/trusted.gpg.d/volian-archive-scar-unstable.gpg > /dev/null


#################################################
#
# Install desired packages
#
# TODO: Separate version for VM instead of personal
#
#################################################

apt update

declare -a apt_deps=(
  "apt-file"
  "autoconf"
  "automake"
  "autopoint"
  "autotools-dev"
  "btrfs-progs"
  "build-essential"
  "byacc"
  "clang-format"
  "cmake"
  "curl"
  "debhelper"
  "dkms"
  "docker"
  "docker-compose"
  "dotnet-sdk-6.0"
  "fakeroot"
  "flex"
  "gengetopt"
  "gettext"
  "git"
  "git-extras"
  "go-md2man"
  "helm"
  "iptables"
  "jq"
  "kubectl"
  "language-support-*"
  "ldap-utils"
  "libassuan-dev"
  "libbtrfs-dev"
  "libbz2-dev"
  "libc6-dev"
  "libdevmapper-dev"
  "libffi-dev"
  "libglib2.0-dev"
  "libgmp3-dev"
  "libgpg-error-dev"
  "libgpgme-dev"
  "libjson-c-dev"
  "liblzma-dev"
  "libncursesw5-dev"
  "libpcap-dev"
  "libprotobuf-c-dev"
  "libprotobuf-dev"
  "libreadline-dev"
  "libseccomp-dev"
  "libselinux1-dev"
  "libsqlite3-dev"
  "libssl-dev"
  "libsystemd-dev"
  "libtool"
  "libunistring-dev"
  "libxml2-dev"
  "libxmlsec1-dev"
  "llvm"
  "make"
  # "nala"  # only for Ubuntu 22.04+
  "nala-legacy"
  "neovim"
  "nikto"
  "nmap"
  "python-dev"
  "python-setuptools"
  "python3-dev"
  "python3-setuptools"
  "pkg-config"
  "podman"  # TODO: steps for podman-compose
  "powershell"
  "rlwrap"
  "runc"
  "tk-dev"
  "uidmap"
  "wget"
  "xz-utils"
  "zlib1g-dev"
  "zsh"
)

# If something's missing, this script starts having some issues...
for pkg in "${apt_deps[@]}"; do
  apt install -y "$pkg" || { echo -e "\n\n[ERROR] Failed to install ${pkg}"; sleep 3; }
done

# But, if you're feeling confident (won't hurt)...
# apt install -y "${apt_deps[@]}"


#################################################
#
# Extra packages from external locations
#
#################################################

# MEGA
wget "https://mega.nz/linux/repo/xUbuntu_20.04/amd64/megacmd-xUbuntu_20.04_amd64.deb" -P /usr/local/pkg/
apt install -y /usr/local/pkg/megacmd-xUbuntu_20.04_amd64.deb
