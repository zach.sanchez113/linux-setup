#!/usr/bin/env zsh

# Use aliases with sudo (not the best in terms of security, so disabling by default)
# alias sudo='sudo '

# Don't add an extra newline before the prompt when clearing
alias clear="unset NEW_LINE_BEFORE_PROMPT && clear"

# Clear both the screen and the scrollback buffer
alias cls='clear && printf "\033c"'

# Never have I ever wanted only bytes
# FS type is useful to have at a glance
alias df="df -hT"

# Sort environment variables by default
alias env="env | sort"

# Make common commands more verbose
alias cp='cp -i -v'
alias mv='mv -i -v'
alias rm='rm -i -v'
alias chown='chown -v'
alias chmod='chmod -v'

# Disable ZSH globs with HTTP commands
# (Future-proofing in case there's an option I missed)
alias curl='noglob curl'
alias wget='noglob wget'
alias xh='noglob xh'

# Enable progress on all rsync transfers
alias rsync='rsync --info=progress2 --info=name0'

# This is kinda annoying when checking to see if I have something installed
if command -v dpkg &>/dev/null; then
  alias dpkg="dpkg --no-pager"
fi

# Better frontend than apt
# NOTE: Doesn't always work quite right with Kali for some reason, so require explicit usage
if command -v apt &>/dev/null && command -v nala &>/dev/null && ! grep -qi kali /etc/os-release 2>/dev/null; then
  alias apt="nala"
fi

# Don't want to use the default timing most of the time
if command -v nmap &>/dev/null; then
  alias nmap='nmap -T4'
fi

# For me personally because my driver installation is busted
# https://github.com/hashcat/hashcat/issues/2628
# if command -v hashcat &>/dev/null; then
#   alias hashcat="hashcat -d 2"
# fi

if command -v exa &>/dev/null; then
  # -F for file type indicators (e.g. '.ssh/' instead of '.ssh'
  # -g because groups aren't shown by default
  alias exa='exa -lagF --time-style=long-iso --git --group-directories-first'
  alias ls='exa'
fi

if command -v bat &>/dev/null; then
  alias cat='bat'
fi

if command -v procs &>/dev/null; then
  alias ps='procs'
fi

if command -v dust &>/dev/null; then
  alias dust='dust -rb'
fi

if command -v btm &>/dev/null; then
  alias top='btm'
fi

if command -v pwsh &>/dev/null; then
  alias powershell='pwsh'
fi

# An OMZ plugin sets duf='du -sh *', so we need to check a static path
if [ -f "${HOME}/.local/bin/duf" ] || [ -f "/usr/bin/duf" ]; then
  unalias duf 2>/dev/null
  alias df='duf'
fi

# Use neovim instead of vim when available
if command -v nvim &>/dev/null; then
  alias vim="nvim"
fi

# Process any extras needed for this specific box/env
if [ -d "${HOME}/.zshalias.d/" ]; then
  find "${HOME}/.zshalias.d/" -type f -print0 | while IFS= read -r -d $'\0' fn; do
    source "${fn}"
  done
fi
