<#
.SYNOPSIS
    Copies settings.json and keybindings.json to the correct place.
#>
function Copy-VSCode-Settings {

  Copy-Item -Path .\vscode-settings.json -Destination $env:APPDATA\Code\User\settings.json
  Copy-Item -Path .\vscode-keybindings.json -Destination $env:APPDATA\Code\User\keybindings.json

}

<#
.SYNOPSIS
    Installs the VS Code extensions I like to use.
#>
function Install-VSCode-Extensions {

  # TODO: Check out cweijan.vscode-database-client2, see if it's better than mtxr.sqltools
  $VSCodeExtensions = @(
    "aaron-bond.better-comments"           # Better Comments
    "adpyke.codesnap"                      # Those pretty code snapshots you see online
    "alefragnani.project-manager"          # Project Manager
    "batisteo.vscode-django"               # Django
    "BazelBuild.vscode-bazel"              # Bazel
    "betwo.vscode-linux-binary-preview"    # Enable viewing of binary file info
    "coolbear.systemd-unit-file"           # systemd unit file support
    "donjayamanne.githistory"              # Better git history
    "eamodio.gitlens"                      # GitLens (extra git functionality)
    "ecmel.vscode-html-css"                # HTML/CSS support
    "EditorConfig.EditorConfig"            # EditorConfig integration
    "esbenp.prettier-vscode"               # Prettier.js in VS Code
    "foxundermoon.shell-format"            # Format shell scripts
    "GitHub.github-vscode-theme"           # Alternative theme based on GitHub, for use in demos and such
    "golang.go"                            # Go support
    # "hashicorp.terraform"                  # Terraform support
    "HookyQR.beautify"                     # Format HTML, CSS, and JS (though prettier is better for CSS and JS)
    "Ionide.Ionide-fsharp"                 # F# support
    "janisdd.vscode-edit-csv"              # Easier editing of CSV files ala Excel
    "formulahendry.auto-rename-tag"        # Automatically rename paired HTML tags
    "khaeransori.json2csv"                 # Convert JSON to CSV (and vice-versa)
    # "lextudio.restructuredtext"            # reStructuredText language server
    "maelvalais.autoconf"                  # Autoconf support
    "max-SS.Cyberpunk"                     # The theme I personally like
    "mechatroner.rainbow-csv"              # Colorize CSVs cause they're hard to read at times
    "mhutchie.git-graph"                   # Git graph view
    "mrmlnc.vscode-scss"                   # SCSS support
    "ms-dotnettools.csharp"                # C# support
    "ms-python.python"                     # Python + friends
    "ms-python.vscode-pylance"             # Pylance (type checking!)
    "ms-toolsai.jupyter"                   # Jupyter
    "ms-toolsai.jupyter-keymap"            # Jupyter
    "ms-toolsai.jupyter-renderers"         # Jupyter
    "ms-vscode-remote.remote-ssh"          # VS Code on a remote machine
    "ms-vscode-remote.remote-ssh-edit"     # SSH config file support
    "ms-vscode.cpptools"                   # C++ support/helpers
    "ms-vscode.makefile-tools"             # Makefile support/helpers
    "ms-vscode.powershell"                 # PowerShell support
    # ! Leaving for posterity, this adds some nasty keybindings
    # "mtxr.sqltools"                        # Run SQL queries
    "nhoizey.gremlins"                     # Avoid unicode "gremlins"
    "njpwerner.autodocstring"              # Easy Python docstrings
    "phplasma.csv-to-table"                # CSV to ASCII table
    "PKief.material-icon-theme"            # Icon theme I prefer
    "richie5um2.vscode-sort-json"          # Sort JSON
    "rust-lang.rust-analyzer"              # Rust support
    "sibiraj-s.vscode-scss-formatter"      # Autoformat SCSS
    "stkb.rewrap"                          # Wrap comments to fit in a certain line length
    "tamasfe.even-better-toml"             # TOML v1+ support (does any official tool care support new versions...?)
    "timonwong.shellcheck"                 # Don't write bad shell scripts
    "trond-snekvik.simple-rst"             # reStructuredText support
    "Vue.volar"                            # Vue.js tooling
    "vscode-icons-team.vscode-icons"       # Alternative icon theme (not my fave, but still looks decent)
    "wix.vscode-import-cost"               # Be careful with how large JS imports can get!
    "yzhang.markdown-all-in-one"           # Extra functionality for MarkDown
  )

  foreach ($ext in $VSCodeExtensions) {
    code --install-extension $ext
  }

}
