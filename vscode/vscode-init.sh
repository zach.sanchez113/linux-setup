#!/usr/bin/env bash


#################################################
#
# Utility functions
#
#################################################

# Write error to stdout then exit 1
#
# Args:
#   message
#
err() {
  echo "${1}" >&2
  echo "aborting" >&2
  exit 1
}

# Check if command exists
#
# Args:
#   command to check existence of
#
check_cmd() {
  command -v "${1:?no command supplied}" > /dev/null 2>&1
}

# Error out if a command doesn't exist
#
# Args:
#   command name
#
need_cmd() {
  if ! check_cmd "${1:?no command supplied}"; then
    err "need '$1' (command not found)"
  fi
}

# Whether this script is being run as root
#
# Returns:
#   0 if true, 1 if false
#
assert_root() {
  if [ $EUID -ne 0 ]; then
    err "This script must be run as root"
  fi
}


#################################################
#
# Ensure VS Code was installed
#
#################################################

if ! check_cmd code; then
  if [ -d ~/.vscode-server ]; then
    alias code=$(find ~/.vscode-server -name "code-server" -executable -print -quit)
  else
    err "Could not locate executable 'code' or 'code-server'"
  fi

elif ! [ -d ~/.vscode-server ] && ! [ -d ~/.config/Code/User ]; then
  err "Could not locate VS Code config directory"

fi


#################################################
#
# Copy over base settings
#
#################################################

if [ -d ~/.config/Code/User ]; then
  cp ./vscode-settings.json ~/.config/Code/User
  cp ./vscode-keybindings.json ~/.config/Code/User
fi


#################################################
#
# Programmatically install VS Code extensions
#
# TODO: Check out cweijan.vscode-database-client2,
# see if it's better than mtxr.sqltools
#
#################################################

declare -a vscode_exts=(
  "aaron-bond.better-comments"           # Better Comments
  "adpyke.codesnap"                      # Those pretty code snapshots you see online
  "alefragnani.project-manager"          # Project Manager
  "batisteo.vscode-django"               # Django
  "BazelBuild.vscode-bazel"              # Bazel
  "betwo.vscode-linux-binary-preview"    # Enable viewing of binary file info
  "coolbear.systemd-unit-file"           # systemd unit file support
  "donjayamanne.githistory"              # Better git history
  "eamodio.gitlens"                      # GitLens (extra git functionality)
  "ecmel.vscode-html-css"                # HTML/CSS support
  "EditorConfig.EditorConfig"            # EditorConfig integration
  "esbenp.prettier-vscode"               # Prettier.js in VS Code
  "foxundermoon.shell-format"            # Format shell scripts
  "GitHub.github-vscode-theme"           # Alternative theme based on GitHub, for use in demos and such
  "golang.go"                            # Go support
  # "hashicorp.terraform"                  # Terraform support
  "HookyQR.beautify"                     # Format HTML, CSS, and JS (though prettier is better for CSS and JS)
  "Ionide.Ionide-fsharp"                 # F# support
  "janisdd.vscode-edit-csv"              # Easier editing of CSV files ala Excel
  "formulahendry.auto-rename-tag"        # Automatically rename paired HTML tags
  "khaeransori.json2csv"                 # Convert JSON to CSV (and vice-versa)
  # "lextudio.restructuredtext"            # reStructuredText language server
  "maelvalais.autoconf"                  # Autoconf support
  "max-SS.Cyberpunk"                     # The theme I personally like
  "mechatroner.rainbow-csv"              # Colorize CSVs cause they're hard to read at times
  "mhutchie.git-graph"                   # Git graph view
  "mrmlnc.vscode-scss"                   # SCSS support
  "ms-dotnettools.csharp"                # C# support
  "ms-python.python"                     # Python + friends
  "ms-python.vscode-pylance"             # Pylance (type checking!)
  "ms-toolsai.jupyter"                   # Jupyter
  "ms-toolsai.jupyter-keymap"            # Jupyter
  "ms-toolsai.jupyter-renderers"         # Jupyter
  "ms-vscode-remote.remote-ssh"          # VS Code on a remote machine
  "ms-vscode-remote.remote-ssh-edit"     # SSH config file support
  "ms-vscode.cpptools"                   # C++ support/helpers
  "ms-vscode.makefile-tools"             # Makefile support/helpers
  "ms-vscode.powershell"                 # PowerShell support
  # ! Leaving for posterity, this adds some nasty keybindings
  # "mtxr.sqltools"                        # Run SQL queries
  "nhoizey.gremlins"                     # Avoid unicode "gremlins"
  "njpwerner.autodocstring"              # Easy Python docstrings
  "phplasma.csv-to-table"                # CSV to ASCII table
  "PKief.material-icon-theme"            # Icon theme I prefer
  "richie5um2.vscode-sort-json"          # Sort JSON
  "rust-lang.rust-analyzer"              # Rust support
  "sibiraj-s.vscode-scss-formatter"      # Autoformat SCSS
  "stkb.rewrap"                          # Wrap comments to fit in a certain line length
  "tamasfe.even-better-toml"             # TOML v1+ support (does any official tool care support new versions...?)
  "timonwong.shellcheck"                 # Don't write bad shell scripts
  "trond-snekvik.simple-rst"             # reStructuredText support
  "Vue.volar"                            # Vue.js tooling
  "vscode-icons-team.vscode-icons"       # Alternative icon theme (not my fave, but still looks decent)
  "wix.vscode-import-cost"               # Be careful with how large JS imports can get!
  "yzhang.markdown-all-in-one"           # Extra functionality for MarkDown
)

for ext in "${vscode_exts[@]}"; do
  code --install-extension "$ext"
done
