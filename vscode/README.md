# VS Code Setup

Yes, settings sync exists, I just haven't bothered to set it up yet due to potential merge conflicts between some of my environments.

## `Ctrl + .` is inserting 'e' (found on Ubuntu 22.04)

This is a new emoji hotkey from IBus. Follow the steps here to disable it: https://stackoverflow.com/a/74314637/11832705

## vscode-init.sh

Just run the script.

## Initialize-VSCode.ps1

Run:

```
. .\Initialize-VSCode.ps1
Copy-VSCode-Settings
Install-VSCode-Extensions
```

## workspace-X.jsonc

Just remove the `workspace-`, change extension to `json`, then add to the root `.vscode` directory. Remove anything that doesn't make sense for the project (e.g. insert final newline when there's a `.editorconfig` file).

For extensions, only `recommendations` is valid. Add anything you want from the extra keys, then remove the extra keys.
