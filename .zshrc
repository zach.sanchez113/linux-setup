#################################################
#
# oh-my-zsh setup
#
#################################################

# In case internet is unavailable, uncomment to disable oh-my-zsh's automatic updates
# DISABLE_AUTO_UPDATE=true

# Path to your oh-my-zsh installation
export ZSH="${HOME}/.oh-my-zsh"

# Theme to use
if [ -f "${ZSH}/custom/themes/z-duellj.zsh-theme" ]; then
  ZSH_THEME="z-duellj"
else
  ZSH_THEME="duellj"
fi

# Which plugins would you like to load?
#
# Standard plugins can be found in $ZSH/plugins/
# Custom plugins may be added to $ZSH_CUSTOM/plugins/
#
# Add wisely, as too many plugins slow down shell startup.
#
plugins=(
  alias-finder
  aliases
  colored-man-pages
  colorize
  common-aliases
  dnf
  docker
  docker-compose
  docker-machine
  gem
  git
  git-extras
  golang
  isodate
  jsontools
  nmap
  nvm
  pip
  poetry
  pyenv
  python
  ripgrep
  ruby
  rust
  safe-paste
  ssh-agent
  systemadmin
  systemd
  ubuntu
  urltools
  vscode
  # NOTE: deprecated, but just in case...
  # yarn
  # TODO: play with this, see if it's worth keeping around
  # zsh-navigation-tools
  zsh-syntax-highlighting
)

# Silence the plugin
zstyle :omz:plugins:ssh-agent quiet yes

# Don't load identities on start
zstyle :omz:plugins:ssh-agent lazy yes

source $ZSH/oh-my-zsh.sh


#################################################
#
# User configuration (see notes for resources)
#
#################################################

# Don't share history between (active) shells
setopt no_share_history
unsetopt share_history

# Turn off history expansion
setopt nobanghist

# ZSH thinks question mark is a glob/wildcard character
unsetopt nomatch

# Don't fail if there's a glob with no matches
setopt nonomatch

# Enable the 'zmv' command, which allows mass-renaming of files
autoload zmv

# Calculator on the command line
autoload zcalc

# Print a newline before the prompt, unless it's the first prompt in the process.
# Credit: https://stackoverflow.com/a/67280892/11832705
function precmd() {
  if [ -z "$NEW_LINE_BEFORE_PROMPT" ]; then
    NEW_LINE_BEFORE_PROMPT=1
  elif [ "$NEW_LINE_BEFORE_PROMPT" -eq 1 ]; then
    echo
  fi
}

# In the event completions need to be manually installed
! [ -d "${ZSH}/completions" ] && mkdir "${ZSH}/completions" 1>/dev/null

# In the event a manpage needs to be manually installed
! [ -d "${ZSH}/man/man1" ] && mkdir -p ${ZSH}/man/man{1,2,3,4,5,6,7,8} 1>/dev/null

# Load nvm completion
[ -s "$NVM_DIR/bash_completion" ] && \. "$NVM_DIR/bash_completion"

# Custom PATH
export PATH="${PATH}:/usr/local/go/bin"
export PATH="${HOME}/go/bin:${PATH}"
export PATH="${HOME}/.local/bin:${PATH}"
export PATH="${HOME}/.nimble/bin:${PATH}"
export PATH="${PATH}:${HOME}/.rvm/bin"

# Load RVM into a shell session *as a function*
# (The plugin doesn't handle this for you)
[[ -s "${HOME}/.rvm/scripts/rvm" ]] && source "${HOME}/.rvm/scripts/rvm"

# Since these can get kinda long
[ -f "${HOME}/.zshalias" ] && . "${HOME}/.zshalias"

# Process any extras needed for this specific box
if [ -d "${HOME}/.zshrc.d/" ]; then
  find "${HOME}/.zshrc.d/" -type f -print0 | while IFS= read -r -d $'\0' fn; do
    source "${fn}"
  done
fi
