# Customized version of duellj theme
# TODO: Clean this up


###########################################################################
#
# Copy of git-prompt-plugin
#
###########################################################################

# Handle $0 according to the standard:
# https://zdharma-continuum.github.io/Zsh-100-Commits-Club/Zsh-Plugin-Standard.html
0="${${ZERO:-${0:#$ZSH_ARGZERO}}:-${(%):-%N}}"
0="${${(M)0:#/*}:-$PWD/$0}"

__GIT_PROMPT_DIR="${0:A:h}"


##################################################
#
# Git-related functions
#
##################################################

# Runs a Python script to print git status info in an easier-to-parse format
# git status --porcelain --branch
#
function update_current_git_vars() {

  unset __CURRENT_GIT_STATUS

  local gitstatus="$__GIT_PROMPT_DIR/gitstatus.py"

  # If the script is acting up
  # _GIT_STATUS=$(python3 ${gitstatus})
  _GIT_STATUS=$(python3 ${gitstatus} 2>/dev/null)

  __CURRENT_GIT_STATUS=("${(@s: :)_GIT_STATUS}")

  GIT_BRANCH=$__CURRENT_GIT_STATUS[1]
  GIT_AHEAD=$__CURRENT_GIT_STATUS[2]
  GIT_BEHIND=$__CURRENT_GIT_STATUS[3]
  GIT_STAGED=$__CURRENT_GIT_STATUS[4]
  GIT_CONFLICTS=$__CURRENT_GIT_STATUS[5]
  GIT_CHANGED=$__CURRENT_GIT_STATUS[6]
  GIT_UNTRACKED=$__CURRENT_GIT_STATUS[7]
  GIT_STASHED=$__CURRENT_GIT_STATUS[8]
  GIT_CLEAN=$__CURRENT_GIT_STATUS[9]

}

# Generates the prompt output
#
function git_super_status() {

  hook_update_git_vars_precmd

  if [ -n "$__CURRENT_GIT_STATUS" ]; then

    # echo -e "\n_GIT_STATUS: $_GIT_STATUS"
    # echo "BRANCH: $GIT_BRANCH"
    # echo "AHEAD: $GIT_AHEAD"
    # echo "BEHIND: $GIT_BEHIND"
    # echo "STAGED: $GIT_STAGED"
    # echo "CONFLICTS: $GIT_CONFLICTS"
    # echo "CHANGED: $GIT_CHANGED"
    # echo "UNTRACKED: $GIT_UNTRACKED"
    # echo "STASHED: $GIT_STASHED"
    # echo "CLEAN: $GIT_CLEAN"

    STATUS="$ZSH_THEME_GIT_PROMPT_PREFIX$ZSH_THEME_GIT_PROMPT_BRANCH$GIT_BRANCH%{${reset_color}%}"

    if [ "$GIT_BEHIND" -ne "0" ]; then
      STATUS="$STATUS $ZSH_THEME_GIT_PROMPT_BEHIND$GIT_BEHIND%{${reset_color}%}"
    fi

    if [ "$GIT_AHEAD" -ne "0" ]; then
      STATUS="$STATUS $ZSH_THEME_GIT_PROMPT_AHEAD$GIT_AHEAD%{${reset_color}%}"
    fi

    STATUS="$STATUS $ZSH_THEME_GIT_PROMPT_SEPARATOR"

    if [ "$GIT_STAGED" -ne "0" ]; then
      STATUS="$STATUS $ZSH_THEME_GIT_PROMPT_STAGED$GIT_STAGED%{${reset_color}%}"
    fi

    if [ "$GIT_CONFLICTS" -ne "0" ]; then
      STATUS="$STATUS $ZSH_THEME_GIT_PROMPT_CONFLICTS$GIT_CONFLICTS%{${reset_color}%}"
    fi

    if [ "$GIT_CHANGED" -ne "0" ]; then
      STATUS="$STATUS $ZSH_THEME_GIT_PROMPT_CHANGED$GIT_CHANGED%{${reset_color}%}"
    fi

    if [ "$GIT_UNTRACKED" -ne "0" ]; then
      STATUS="$STATUS $ZSH_THEME_GIT_PROMPT_UNTRACKED$GIT_UNTRACKED%{${reset_color}%}"
    fi

    if [ "$GIT_STASHED" -ne "0" ]; then
      STATUS="$STATUS $ZSH_THEME_GIT_PROMPT_STASHED$GIT_STASHED%{${reset_color}%}"
    fi

    if [ "$GIT_CLEAN" -eq "1" ]; then
      STATUS="$STATUS $ZSH_THEME_GIT_PROMPT_CLEAN%{${reset_color}%}"
    fi

    STATUS="$STATUS%{${reset_color}%}$ZSH_THEME_GIT_PROMPT_SUFFIX"
    echo "$STATUS"

  # Not in a git repo
  else
    unset ZLE_RPROMPT_INDENT

  fi

}


##################################################
#
# ZSH hooks
#
##################################################

function hook_update_git_vars_chpwd() {
  update_current_git_vars
}

function hook_update_git_vars_preexec() {
  case "$2" in
    git*|hub*|gh*|stg*)
      __EXECUTED_GIT_COMMAND=1
    ;;
  esac
}

function hook_update_git_vars_precmd() {
  if [ -n "$__EXECUTED_GIT_COMMAND" ] || [ ! -n "$ZSH_THEME_GIT_PROMPT_CACHE" ]; then
    update_current_git_vars
    unset __EXECUTED_GIT_COMMAND
  fi
}

autoload -U add-zsh-hook
add-zsh-hook chpwd hook_update_git_vars_chpwd
add-zsh-hook precmd hook_update_git_vars_precmd
add-zsh-hook preexec hook_update_git_vars_preexec


###########################################################################
#
# Begin custom theme
#
###########################################################################

# Colors because I didn't know about the built-in ones when I wrote this
# See: https://gist.github.com/JBlond/2fea43a3049b38287e5e9cefc87b2124
_yellow=$'%{\e[0;33m%}'
_blue=$'%{\e[0;34m%}'
_cyan=$'%{\e[0;36m%}'
_bold_green=$'%{\e[1;32m%}'
_bold_blue=$'%{\e[1;34m%}'
_bold_purple=$'%{\e[1;35m%}'
_bold_white=$'%{\e[1;37m%}'
_reset_color=$'%{\e[0m%}'

# Helpers for prompt expansion
# See: https://zsh.sourceforge.io/Doc/Release/Prompt-Expansion.html
_start_bold=$'%B'
_end_bold=$'%b'
_dt_24h=$'%*'
_username=$'%n'
_hostname_trunc=$'%m'
_dollar_or_bang=$'%(!.#.$)'
_hist_event_num=$'%!'
_pwd=$'%~'

# Cause this repeats a lot
_open="${_blue}${_start_bold}[${_end_bold}${_reset_color}"
_close="${_blue}${_start_bold}]${_end_bold}${_reset_color}"
_open_paren="${_blue}${_start_bold}(${_end_bold}${_reset_color}"
_close_paren="${_blue}${_start_bold})${_end_bold}${_reset_color}"


##################################################
#
# Functions
#
##################################################

# Calculate prompt length even for Unicode characters (original excluded them)
# Credit: https://stackoverflow.com/a/57141646
#
function prompt_length() {
  emulate -L zsh
  local COLUMNS=${2:-$COLUMNS}
  local -i x y=${#1} m
  if (( y )); then
    while (( ${${(%):-$1%$y(l.1.0)}[-1]} )); do
      x=y
      (( y *= 2 ))
    done
    while (( y > x + 1 )); do
      (( m = x + (y - x) / 2 ))
      (( ${${(%):-$1%$m(l.x.y)}[-1]} = m ))
    done
  fi
  echo $x
}

# Determine whitespace needed to pad two different strings
# Credit: buereau theme
#
function get_space() {

  local LENGTH=$(prompt_length "$1$2")
  local SPACES=$(( COLUMNS - LENGTH - ${ZLE_RPROMPT_INDENT:-1} ))

  (( SPACES > 0 )) || return
  printf ' %.0s' {1..$SPACES}

}

# Print out the venv if it exists
#
function _venv_prompt() {

  if ! [ -z $VIRTUAL_ENV ]; then

    # I always have the poetry venv in the project directory, so I need a way to know which directory the venv
    # is from
    if (( ${+POETRY_ACTIVE} )) && [ $POETRY_ACTIVE -eq 1 ]; then
      venv_name=$(basename "${VIRTUAL_ENV}")
      venv_dir=$(basename $(dirname "${VIRTUAL_ENV}"))
      echo " ${_blue}${_start_bold}(${_end_bold}${_yellow}${venv_name} | ${venv_dir}${_blue}${_start_bold})${_end_bold}${_reset_color}"

    # If the venv isn't managed by poetry, just fall back to a more standard venv prompt
    else
      venv_name="${VIRTUAL_ENV##*/}"
      echo " ${_blue}${_start_bold}(${_end_bold}${_yellow}${venv_name}${_blue}${_start_bold})${_end_bold}${_reset_color}"

    fi

  fi

}


##################################################
#
# Prompt settings
#
##################################################

# Settings for git_prompt_info
ZSH_THEME_GIT_PROMPT_PREFIX=" ${_blue}${_start_bold}-${_end_bold}${_reset_color} ${_open} "
ZSH_THEME_GIT_PROMPT_SUFFIX=" ${_close}%{$reset_color%}"
ZSH_THEME_GIT_PROMPT_BRANCH="${_bold_green}"

# Default values for the appearance of the prompt from git-prompt plugin.
# ZSH_THEME_GIT_PROMPT_PREFIX="("
# ZSH_THEME_GIT_PROMPT_SUFFIX=")"
ZSH_THEME_GIT_PROMPT_SEPARATOR="${_blue}${_start_bold}|${_end_bold}${_reset_color}"
# ZSH_THEME_GIT_PROMPT_BRANCH="%{$fg_bold[magenta]%}"
ZSH_THEME_GIT_PROMPT_STAGED="%{$fg[red]%}%{●%G%}"
ZSH_THEME_GIT_PROMPT_CONFLICTS="%{$fg[red]%}%{✖%G%}"

# Uncomment the bottom one if it doesn't render properly
ZSH_THEME_GIT_PROMPT_CHANGED="%{$fg[blue]%}%{✚%G%}${_reset_color}"
ZSH_THEME_GIT_PROMPT_CHANGED="%{$fg[blue]%}%{+%G%}"

ZSH_THEME_GIT_PROMPT_BEHIND="%{↓%G%}"
ZSH_THEME_GIT_PROMPT_AHEAD="%{↑%G%}"
ZSH_THEME_GIT_PROMPT_UNTRACKED="%{$fg[cyan]%}%{?%G%}"
ZSH_THEME_GIT_PROMPT_STASHED="%{$fg_bold[blue]%}%{⚑%G%}"
ZSH_THEME_GIT_PROMPT_CLEAN="%{$fg_bold[green]%}%{✔%G%}"


##################################################
#
# Do the thing
#
##################################################

# Basically, duellj has the current time as RPROMPT, which I don't want, so split out the first line
# and print it before $PROMPT (the second line)
_zach_theme_precmd() {

  if [ "${USER:-}" = "root" ]; then
    _username_colored="${_bold_red}${_username}${_reset_color}"
  else
    _username_colored="${_bold_green}${_username}${_reset_color}"
  fi

  _leading="${_blue}${_start_bold}┌─"
  _user_host="${_open}${_bold_green}${_username_colored}${_bold_blue}@${_reset_color}${_cyan}${_hostname_trunc}${_close}"
  _dir="${_open}${_bold_white}${_pwd}${_close}"

  _prompt_header="${_leading}${_user_host}$(_venv_prompt) ${_blue}${_start_bold}-${_end_bold}${_reset_color} ${_dir}$(git_super_status)"

  spaces=$(get_space "${_prompt_header}" "[${_dt_24h}]")
  print -rP "${_prompt_header}${spaces}[${_dt_24h}]"

}

# Enable command subsitution in the prompt
setopt prompt_subst

# Main prompt line
PROMPT="${_blue}${_start_bold}└─${_open}${_bold_purple}${_dollar_or_bang}${_close} "

# What shows up on continuation line
PS2=$' \e[0;34m%}${_start_bold}>${_reset_color}${_end_bold} '

# Inject prompt header in precmd hook
# autoload -U add-zsh-hook
add-zsh-hook precmd _zach_theme_precmd
