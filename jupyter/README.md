# Jupyter Lab

## Setup

Basically, create a dedicated notebook directory `~/jupyter/`, create a dedicated `files/` directory to store any external files/data, a `templates/` directory for any notebook templates. Then of course set up the systemd service so we don't have to run this in a terminal/keep one open at all times.

Credits for the systemd service file: https://janakiev.com/blog/jupyter-systemd/

**WARNING:** `requirements.txt` may not be 100% - install new modules as needed.

```bash
mkdir -p ~/jupyter/files ~/jupyter/templates
cp ./templates/base.ipynb ~/jupyter/templates/

# Don't forget to change the home directory in the sample config file!
jupyter lab --generate-config
cat ./jupyter_lab_config.py >> ~/.jupyter/jupyter_lab_config.py

# This will help prevent issues with tokens
jupyter lab password

# This service file may move around
cp jupyter.service /etc/systemd/system/
systemctl daemon-reload
systemctl enable jupyter
systemctl start jupyter
```

## Base Template

**TODO:** How to maintain this?

This contains a number of utility functions, config, and pre-loaded functions that I personally like to have available whenever I start a new notebook.

To import/execute everything in your own notebook, run:

```
%run ~/jupyter/templates/base.ipynb import *
```
