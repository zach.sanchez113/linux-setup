#!/usr/bin/env bash

# In case a distro breaks PATH when sourcing /etc/profile (looking at you, Kali)
#
pathmunge() {
  if ! echo $PATH | /bin/egrep -q "(^|:)$1($|:)"; then
    if [ "$2" = "after" ]; then
      PATH=$PATH:$1
    else
      PATH=$1:$PATH
    fi
  fi
}
