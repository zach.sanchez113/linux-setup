#!/usr/bin/env bash

set -e
set -u

# Source: https://opensource.com/article/21/1/go-cross-compiling
# Also see: https://golangcookbook.com/chapters/running/cross-compiling/
archs=(amd64 arm64 ppc64le ppc64 s390x)
scriptname="${1:?missing script name}"

for arch in "${archs[@]}"; do
	env GOOS=linux GOARCH="${arch}" go build -o "${scriptname}_${arch}"
done
