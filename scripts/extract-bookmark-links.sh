#!/usr/bin/env bash

####################################################################################################
#
# Extract links from HTML export of bookmarks in Markdown format
# May require some manual cleaning
#
####################################################################################################

set -e
set -u

pcregrep -o1 "(<A.*</A>)" "${1:?missing bookmarks export}" \
  | pcregrep -o1 -o2 ".*(HREF=\"[^\s]*\") .*>(.*)</A>" \
  | awk -F '"' '{ print "- [" $3 "](" $2 ")" }'
