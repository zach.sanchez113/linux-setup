#!/usr/bin/env bash

svc_file="${1:?please pass in a systemd service file as the first argument}"

if ! [ -f "${svc_file}" ]; then
  echo -e "${svc_file} does not exist" >&2
  exit 1
fi

# For anything installed by a package manager, use /usr/lib/systemd/system
# See: https://unix.stackexchange.com/a/458252
sudo cp "${svc_file}" /etc/systemd/system
