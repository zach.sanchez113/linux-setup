#!/usr/bin/env bash

# Create dotfile for formatting
# Be sure to set `ColumnLimit: 110`
clang-format --dump-config --style=LLVM > ./clang-format

# Perform the formatting
clang-format -i src/*
