#!/usr/bin/env zsh

##############################################################
#
# ! More up-to-date version is in security-experiments repo.
# ! Just leaving this here so I don't forget.
#
##############################################################

mkdir -p /usr/local/pkg/{musl,gnu}

# ! Using musl builds to prevent glibc compatibility issues
# This won't work if native libraries are needed, though - in that case, use a container w/ an older glibc
# https://kobzol.github.io/rust/ci/2021/05/07/building-rust-binaries-in-ci-that-work-with-older-glibc.html

wget -O /usr/local/pkg/musl/exa-v0.10.1.zip \
  https://github.com/ogham/exa/releases/download/v0.10.1/exa-linux-x86_64-musl-v0.10.1.zip

unzip -p /usr/local/pkg/musl/exa-v0.10.1.zip bin/exa > /usr/local/pkg/musl/exa
chmod +x /usr/local/pkg/musl/exa

wget -O /usr/local/pkg/musl/bat-v0.22.0.tar.gz \
  https://github.com/sharkdp/bat/releases/download/v0.22.0/bat-v0.22.0-x86_64-unknown-linux-musl.tar.gz

tar xvzf /usr/local/pkg/musl/bat-v0.22.0.tar.gz bat-v0.22.0-x86_64-unknown-linux-musl/bat -C /usr/local/pkg/musl/ --strip-components=1

wget -O /usr/local/pkg/musl/zellij-v0.31.3.tar.gz \
  https://github.com/zellij-org/zellij/releases/download/v0.31.3/zellij-x86_64-unknown-linux-musl.tar.gz

tar xvzf /usr/local/pkg/musl/zellij-v0.31.3.tar.gz zellij -C /usr/local/pkg/musl/

wget -O /usr/local/pkg/musl/ripgrep-13.0.0.tar.gz \
    https://github.com/BurntSushi/ripgrep/releases/download/13.0.0/ripgrep-13.0.0-x86_64-unknown-linux-musl.tar.gz

tar xvzf /usr/local/pkg/musl/ripgrep-13.0.0.tar.gz ripgrep-13.0.0-x86_64-unknown-linux-musl/rg -C /usr/local/pkg/musl/ --strip-components=1

wget -O /usr/local/pkg/musl/du-dust-v0.8.3.tar.gz \
    https://github.com/bootandy/dust/releases/download/v0.8.3/dust-v0.8.3-x86_64-unknown-linux-musl.tar.gz

tar xvzf /usr/local/pkg/musl/du-dust-v0.8.3.tar.gz dust-v0.8.3-x86_64-unknown-linux-musl/dust -C /usr/local/pkg/musl/ --strip-components=1

wget -O /usr/local/pkg/musl/tealdeer-v1.5.0 \
    https://github.com/dbrgn/tealdeer/releases/download/v1.5.0/tealdeer-linux-x86_64-musl

cp /usr/local/pkg/musl/tealdeer-v1.5.0 /usr/local/pkg/musl/tealdeer
chmod +x /usr/local/pkg/musl/tealdeer

wget -O /usr/local/pkg/musl/bottom-0.6.8.tar.gz \
    https://github.com/ClementTsang/bottom/releases/download/0.6.8/bottom_x86_64-unknown-linux-musl.tar.gz

tar xvzf /usr/local/pkg/musl/bottom-0.6.8.tar.gz btm -C /usr/local/pkg/musl/

# TODO: Is this musl?
wget -O /usr/local/pkg/musl/procs-v0.13.0.zip \
    https://github.com/dalance/procs/releases/download/v0.13.0/procs-v0.13.0-x86_64-linux.zip

unzip -p /usr/local/pkg/musl/procs-v0.13.0.zip procs > /usr/local/pkg/musl/procs
chmod +x /usr/local/pkg/musl/procs

chown -R root:root /usr/local/pkg/musl/*
