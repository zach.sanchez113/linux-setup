#!/usr/bin/env bash

# The GUI may fail to render when in a sandbox, so...
anki --no-sandbox
