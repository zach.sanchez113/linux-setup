#!/usr/bin/env bash

##################################################
#
# Example rsync usage
#
##################################################

# If desired, this will enable progress on all transfers
# Excluding from commands to reduce
alias rsync='rsync --info=progress2 --info=name0'

# For copying files on the same system:
rsync -avz \
  --exclude-from ./rsync-exclusions.txt \
  /path/to/src/* /path/to/dst/

# For copying files over SSH (can use usual SSH args in command):
rsync -avze 'ssh' \
  --exclude-from ./rsync-exclusions.txt \
  /path/to/src/* $USER@$EXAMPLE_HOST:/tmp/same-dirname/

# With more verbose arguments:
#
# NOTE: [-a, --archive] is used to create a backup of the files + directory structure, short for -rlptgoD
#
rsync \
  --archive \
  --verbose \
  --compress \
  --rsh 'ssh' \
  --exclude-from ./rsync-exclusions.txt \
  /path/to/src/* \
  $USER@$EXAMPLE_HOST:/tmp/same-dirname/
