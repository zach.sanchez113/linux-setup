#!/usr/bin/env bash


#################################################
#
# Helpers/big functions
#
#################################################

# Helper to check command existence
check_cmd() {
  command -v "$1" > /dev/null 2>&1
}

# Install dependencies for Debian-based distros
#
install_prereq_deb() {

  sudo apt install \
    btrfs-progs \
    git \
    golang-go \
    go-md2man \
    iptables \
    libassuan-dev \
    libbtrfs-dev \
    libc6-dev \
    libdevmapper-dev \
    libglib2.0-dev \
    libgpgme-dev \
    libgpg-error-dev \
    libprotobuf-dev \
    libprotobuf-c-dev \
    libseccomp-dev \
    libselinux1-dev \
    libsystemd-dev \
    pkg-config \
    runc \
    uidmap

}

# Install dependencies for RHEL-based distros
#
install_prereq_rhel() {

  if check_cmd dnf; then
    installer=yum
  else
    installer=dnf
  fi

  sudo $installer install -y \
    btrfs-progs-devel \
    conmon \
    containernetworking-plugins \
    containers-common \
    crun \
    device-mapper-devel \
    git \
    glib2-devel \
    glibc-devel \
    glibc-static \
    go \
    golang-github-cpuguy83-md2man \
    gpgme-devel \
    iptables \
    libassuan-devel \
    libgpg-error-devel \
    libseccomp-devel \
    libselinux-devel \
    make \
    pkgconfig

}


#################################################
#
# Actual steps for compiling Podman
#
# Reference:
# https://podman.io/getting-started/installation#building-missing-dependencies
#
#################################################

# Platform-specific installation of dependencies
#
# Reference:
# https://podman.io/getting-started/installation#build-and-run-dependencies
#
install_prereqs() {

  if check_cmd apt; then
    install_prereq_deb
  elif check_cmd yum || check_cmd dnf; then
    install_prereq_rhel
  fi

}

# Install or upgrade go if needed (requires >= 1.16)
#
# TODO: Warn if upgrading go?
#
install_golang() {

  # Extract version number from 'go version' output
  declare go_version=$(go version 2>/dev/null | awk -F " " '{ substr($3,3); }' || echo "")

  # Check if variable is empty
  # If not empty, use a hacky regex to figure out if an upgrade is needed (match 0.x, 1.0x, 1.10-1.15)
  if [ -z $go_version ] || echo "$go_version" | grep -q -P "^0|^1\.0|^1\.1[0-5]"; then
    curl --location --silent --show-error https://go.dev/dl/go1.17.7.linux-amd64.tar.gz | sudo tar -xzf - -C /usr/local/
  fi

  mkdir -p "${GOPATH:-$HOME/go}/bin" "${GOPATH:-$HOME/go}/pkg" "${GOPATH:-$HOME/go}/src"

}

# Install latest version of conmon
#
compile_conmon() {

  git clone https://github.com/containers/conmon
  pushd conmon
  export GOCACHE="$(mktemp -d)"
  make
  sudo make podman
  popd

}

# Install latest version of runc
#
compile_runc() {

  git clone https://github.com/opencontainers/runc.git "${GOPATH:-$HOME/go}/src/github.com/opencontainers/runc"
  pushd "${GOPATH:-$HOME/go}/src/github.com/opencontainers/runc"
  make BUILDTAGS="selinux seccomp"
  sudo cp runc /usr/bin/runc
  popd

}

# Configure Container Network Interface (CNI)
#
# TODO: This may need some tweaking, assuming it's even needed
# See: https://github.com/containers/podman/blob/main/cni/README.md
#
configure_cni() {

  sudo mkdir -p /etc/cni/net.d
  curl -qsSL https://raw.githubusercontent.com/containers/podman/main/cni/87-podman-bridge.conflist \
    | sudo tee /etc/cni/net.d/87-podman-bridge.conflist

}

# Get or update configuration files for Podman
#
fetch_container_config() {

  # Back up old ones if needed
  [ -f /etc/containers/registries.conf ] && mv /etc/containers/registries.conf /etc/containers/registries.conf.bak
  [ -f /etc/containers/policy.json ] && mv /etc/containers/policy.json /etc/containers/policy.json.bak

  sudo mkdir -p /etc/containers
  sudo curl -L -o /etc/containers/registries.conf https://src.fedoraproject.org/rpms/containers-common/raw/main/f/registries.conf
  sudo curl -L -o /etc/containers/policy.json https://src.fedoraproject.org/rpms/containers-common/raw/main/f/default-policy.json

}

# Compile/install Podman
#
# TODO: BUILDTAGS isn't strictly necessary, but default BUILDTAGS="selinux seccomp" - dig into this
#
compile_podman() {

  git clone https://github.com/containers/podman/
  pushd podman
  cd podman
  make BUILDTAGS=""
  sudo make install PREFIX=/usr
  popd

}

# I'd like to have this available as a lib I can execute step-by-step just in case, so this is
# effectively the "main" function
#
setup_latest_podman() {

  # Before anything, let's make a source directory to prevent clutter...
  mkdir -p "${HOME}/.local/src/"
  pushd "${HOME}/.local/src/"

  # Step-by-step from the official guide
  install_prereqs
  install_golang
  compile_conmon
  compile_runc

  # TODO: Re-enable if actually needed
  # configure_cni

  fetch_container_config
  compile_podman

  # Now back to the original directory
  popd
}
