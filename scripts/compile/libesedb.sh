#!/usr/bin/env bash


#################################################
#
# Helpers/big functions
#
#################################################

# Helper to check command existence
#
check_cmd() {
  command -v "$1" > /dev/null 2>&1
}

# Install dependencies for Debian-based distros
#
install_prereq_deb() {

  sudo apt install -y \
    git \
    autoconf \
    automake \
    autopoint \
    libtool \
    pkg-config \
    gettext

}

# Install dependencies for RHEL-based distros
#
install_prereq_rhel() {

  if check_cmd dnf; then
    installer=yum
  else
    installer=dnf
  fi

  # TODO: Could also be 'sudo dnf install git python3-devel autoconf automake gettext-devel libtool pkg-config'
  sudo $installer install -y \
    git \
    python36-devel \
    automake \
    gettext \
    gettext-devel \
    pkgconf-pkg-config \
    libtool \
    rpm-build

}


#################################################
#
# Actual steps for compiling libesedb
#
# Reference:
# https://github.com/libyal/libesedb/wiki/Building
#
#################################################

# Platform-specific installation of dependencies
# NOTE: Only necessary if not using source distribution package
#
install_prereqs() {

  if check_cmd apt; then
    install_prereq_deb
  elif check_cmd yum || check_cmd dnf; then
    install_prereq_rhel
  fi

}

clone_git_repo() {

  sudo git clone https://github.com/libyal/libesedb.git /usr/local/src
  pushd /usr/local/src/libesedb/
  sudo ./synclibs.sh
  sudo ./autogen.sh
  popd

}

download_source_dist() {

  curl -sSL https://github.com/libyal/libesedb/releases/download/20220806/libesedb-experimental-20220806.tar.gz \
    | tar -xvfz - -C /usr/local/src/

}

install_libesedb() {

  pushd /usr/local/src/libesedb
  ./configure --enable-python3
  make
  sudo make install
  sudo ldconfig
  python setup.py build
  python setup.py install
  popd

}

# ! WARNING: While not *strictly* necessary, libesedb should be installed before running
#
install_pyesedb() {

  pushd /usr/local/src/libesedb
  python setup.py build
  python setup.py install
  popd

}
