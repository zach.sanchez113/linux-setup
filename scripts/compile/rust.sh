#!/usr/bin/env bash

compile_rust() {

  curl --location --silent --show-error  https://static.rust-lang.org/dist/rust-1.61.0-x86_64-unknown-linux-gnu.tar.gz \
    | sudo tar xvf - -C /usr/local/src/

  pushd /usr/local/src/rust-1.61.0-x86_64-unknown-linux-gnu/
  sudo ./install.sh
  popd

}
