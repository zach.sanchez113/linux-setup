#!/usr/bin/env bash

# Compile Python from source
#
# This is an optimized build w/ SQLite extensions
#
# Args:
#   version number, e.g. 3.7.2
#
compile_python() {

  declare -r py_version="${1:?missing python version}"

  curl -sSL "https://www.python.org/ftp/python/${py_version}/Python-${py_version}.tgz" | sudo tar xvf - -C /usr/local/src/

  pushd "/usr/local/$py_version"
  ./configure --prefix="/usr/local/python-$py_version" --enable-loadable-sqlite-extensions --enable-optimizations
  make
  sudo make altinstall
  popd

}
