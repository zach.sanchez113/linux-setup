#!/usr/bin/env bash

compile_redis() {

  curl -sSL https://download.redis.io/redis-stable.tar.gz | sudo tar xvf - -C /usr/local/src/
  make -C /usr/local/src/redis-stable
  sudo make install -C /usr/local/src/redis-stable

}
