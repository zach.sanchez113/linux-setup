#!/usr/bin/env bash

# Bash strict mode
# See: http://redsymbol.net/articles/unofficial-bash-strict-mode/
set -e
set -u
set -o pipefail
shopt -s failglob


#################################################
#
# Install golang from source
#
# Just a one-off function for my own reference,
# pulled from Podman's manual compilation doc
#
# Probably will never actually use this though,
# gvm is way easier
#
# TODO: Not 100% sure if this is bootstrapping right
#
# ! WARNING: Need to checkout a specific tag
# ! if you don't wanna use whatever this ends
# ! up cloning by default
#
#################################################

compile_golang() {

  export GOPATH=~/go
  git clone https://go.googlesource.com/go $GOPATH
  pushd "$GOPATH/src/"
  ./all.bash
  export PATH=$GOPATH/bin:$PATH
  popd

  echo -e "\n[$(date)] Done!"
  echo -e "\n[$(date)] Don't forget to fix up the PATH + GOPATH in your rcfiles to point to the bootstrapped version!"

}
