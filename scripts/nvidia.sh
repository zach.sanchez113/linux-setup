#!/usr/bin/env bash


install_deb() {

  declare -a pkg_list=(
    "dkms"
    "linux-headers-$(uname -r)"
    "nvidia-driver"
  )

  sudo apt install -y "${pkg_list[@]}"

  # Or (haven't tested this!):
  sudo ubuntu-drivers autoinstall

}

# TODO: Is there even a way to do this from repos?
install_rhel() {

  declare -a pkg_list=(
    "dkms"
  )

  sudo yum install -y "${pkg_list[@]}"

}
