#!/usr/bin/env bash

set -e
set -u
set -o pipefail
shopt -s failglob


# Make sure the user is root
root_only() {

  if [ "$EUID" -ne 0 ]; then
    echo "Please run as root"
    exit 1
  fi

}

# Get a user's home directory
#
# Args:
# - Username
#
homedir() {
  _home=$(eval echo "~${1:?no username specified}")
}

# Clone git repo, place in /usr/local/src
#
# Args:
# - Git repo to clone (same as you'd use for 'git clone')
#
git_clone_src() {

  git_repo="${1:?A git repo must be specified.}"
  tool_name="${git_repo##*/}"
  tool_name="${tool_name%.git}"
  src_dir="/usr/local/src/${tool_name}"

  mkdir "${src_dir}"
  git clone "$git_repo" "${src_dir}" || {
    echo "ERROR: Couldn't clone ${git_repo} to ${src_dir}!" 1>&2;
    exit 1;
  }

  echo "${src_dir}"

}

# Add appropriate shebang to Python script
#
# Args:
# - File to fix
#
prepend_py_shebang() {
  sed -i '1s/^/#!/usr/bin/env python3\n\n/' "${1}"
}

# Install a standalone Python script from git (placed in /usr/local/bin/)
#
# Args:
# - Git repo to clone (same as you'd use for 'git clone')
#
install_python_bin() {

  git_repo="${1:?A git repo must be specified.}"
  tool_name="${git_repo##*/}"
  tool_name="${tool_name%.git}"
  src_dir="/usr/local/src/${tool_name}"

  git_clone_src "${git_repo}"

  [ -f "${src_dir}/requirements.txt" ] && /usr/bin/env python3 -m pip install "${src_dir}/requirements.txt";

  if [ -f "${src_dir}/${tool_name}.py" ]; then
    prepend_py_shebang "${src_dir}/${tool_name}.py";
    ln -sf "${src_dir}/${tool_name}.py" /usr/local/bin/;
    chmod a+x "/usr/local/bin/${tool_name}.py";
  else
    echo "ERROR: No file named ${src_dir}/${tool_name}.py! Check filename, and then symlink manually." 1>&2
  fi

}
