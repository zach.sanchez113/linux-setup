#!/usr/bin/env bash

install_port_scanners() {
  apt update && apt install -y \
    nmap build-essential cmake libgmp3-dev gengetopt libpcap-dev flex byacc libjson-c-dev \
    pkg-config libunistring-dev

  podman pull rustscan/rustscan

  git_clone_src https://github.com/zmap/zmap.git
  git_clone_src https://github.com/robertdavidgraham/masscan.git

  { cd /usr/local/src/zmap && cmake . && make -j4 && make install; }
  { cd /usr/local/src/masscan && make && make install; }
}


install_zdns() {
  git_clone_src https://github.com/zmap/zdns.git
  { cd /usr/local/src/zdns/zdns/ && /usr/bin/env go build; }
  ln -sf /usr/local/src/zdns/zdns/zdns /usr/local/bin/
}


install_python_stuff() {
  /usr/bin/env python3 -m pip install \
    -r ./requirements-infosec-tools.txt \
    -r ./requirements-rephraser.txt

  install_python_bin https://github.com/maurosoria/dirsearch.git
  install_python_bin https://github.com/ropnop/windapsearch.git
  install_python_bin https://github.com/initstring/cloud_enum.git
  install_python_bin https://github.com/christophetd/cloudflair.git
  install_python_bin https://github.com/travco/rephraser.git
}


install_seclists() {
  mkdir /usr/local/share/SecLists \
    /usr/local/share/default-creds

  git clone https://github.com/danielmiessler/SecLists.git /usr/local/share/SecLists
  git clone https://github.com/ihebski/DefaultCreds-cheat-sheet.git /usr/local/share/default-creds
}


install_hashcat() {
  git_clone_src https://github.com/hashcat/hashcat.git
  git_clone_src https://github.com/hashcat/hashcat-utils.git
  git_clone_src https://github.com/hashcat/princeprocessor.git
  git_clone_src https://github.com/initstring/passphrase-wordlist.git  # TODO: This isn't awesome

  { cd /usr/local/src/hashcat && make && make install; }
  { cd /usr/local/src/hashcat-utils && make; }
  { cd /usr/local/src/princeprocessor && make; }

  find /usr/local/src/hashcat-utils/bin/ -type f -exec cp -s {} /usr/local/bin/ \;
  ln -sf /usr/local/src/princeprocessor/src/pp64.bin /usr/local/bin/
}


install_sniffers() {
  apt update && apt install -y libpcap-dev libseccomp-dev

  [ -f "${HOME:-/home/$USER}/.cargo/env" ] && source "${HOME:-/home/$USER}/.cargo/env"
  cargo install sniffglue
  find "${HOME:-/home/$USER}/.cargo/bin/" -type f -exec cp -s {} /usr/local/bin/ \;
}


pull_podman_images() {
  podman pull zricethezav/gitleaks:latest
}


install_osint_tools() {
  install_python_bin https://github.com/lanmaster53/recon-ng.git
  /usr/bin/env python3 -m pip install -r /usr/local/src/recon-ng/REQUIREMENTS

  # Manual steps for actual script, hence stderr suppression
  install_python_bin https://github.com/owasp/D4N155.git 2>/dev/null
  prepend_py_shebang /usr/local/src/D4N155/D4N155.py
  chmod a+x /usr/local/src/D4N155/D4N155.py
  ln -sf /usr/local/src/D4N155/D4N155.py /usr/local/bin/
}


# TODO: rvm instead of ruby
install_webapp_tools() {
  apt update && apt install nikto ruby ruby-dev

  git_clone_src https://github.com/zmap/zgrab2.git
  git_clone_src https://github.com/AliasIO/wappalyzer.git

  git clone https://github.com/zmap/zgrab2.git /usr/local/src/zgrab2
  { cd /usr/local/src/zgrab2 && make; }
  ln -sf /usr/local/src/zgrab2/cmd/zgrab2/zgrab2 /usr/local/bin/

  echo 'deb http://download.opensuse.org/repositories/home:/cabelo/xUbuntu_20.04/ /' | tee /etc/apt/sources.list.d/home:cabelo.list
  curl -fsSL https://download.opensuse.org/repositories/home:cabelo/xUbuntu_20.04/Release.key \
    | gpg --dearmor \
    | tee /etc/apt/trusted.gpg.d/home_cabelo.gpg > /dev/null
  apt update && apt install owasp-zap

  go get -v github.com/OWASP/Amass/v3/...
  ln -sf "${GOPATH:-$HOME/go}/bin/amass" /usr/local/bin/

  { cd /usr/local/src/wappalyzer && yarn install && yarn link; }
  ln -sf /usr/local/src/wappalyzer/src/driver/npm/cli.js /usr/local/bin/wappalyzer

  wget https://github.com/findomain/findomain/releases/latest/download/findomain-linux -O /usr/local/bin/findomain
  chmod a+x /usr/local/bin/findomain

  command -v gem >/dev/null || { echo "TODO: Ensure RubyGems is installed." >&2; exit 1; }
  gem install wpscan

  git_clone_src https://github.com/vanhauser-thc/thc-hydra.git
  { cd /usr/local/src/thc-hydra && ./configure && make && make install; }
}


install_hakrevdns() {
  go get github.com/hakluke/hakrevdns
  ln -sf "${GOPATH:-$HOME/go}/bin/hakrevdns" /usr/local/bin/
}


clone_linux_lpe() {
  git_clone_src https://github.com/AlessandroZ/BeRoot.git
  git_clone_src https://github.com/rebootuser/LinEnum.git
  git_clone_src https://github.com/mzet-/linux-exploit-suggester.git
  git_clone_src https://github.com/TH3xACE/SUDO_KILLER.git
  git_clone_src https://github.com/Anon-Exploiter/SUID3NUM.git

  # ! golang
  # Download binary from here: https://github.com/DominicBreuker/pspy/releases
  git_clone_src https://github.com/DominicBreuker/pspy.git

  # ! golang
  # Download binary from here: https://github.com/liamg/traitor/releases
  git_clone_src https://github.com/liamg/traitor.git
  CGO_ENABLED=0 go get -u github.com/liamg/traitor/cmd/traitor
}


clone_windows_lpe() {
  git_clone_src https://github.com/AlessandroZ/BeRoot.git
  git_clone_src https://github.com/hlldz/Phant0m.git

  # pypykatz should really be a pip install
  git_clone_src https://github.com/skelsec/pypykatz.git

  # Download binary from here: https://github.com/gentilkiwi/mimikatz/releases
  git_clone_src https://github.com/ParrotSec/mimikatz.git
}


download_windows_misc() {
  # Original tools are deprecated:
  # git_clone_src https://github.com/EmpireProject/Empire.git
  # git_clone_src https://github.com/PowerShellMafia/PowerSploit.git
  git_clone_src https://github.com/BC-SECURITY/Empire.git
}


download_linux_misc() {
  # ldapsearch
  apt update && apt install ldap-utils

  git_clone_src https://github.com/blendin/3snake.git
}
