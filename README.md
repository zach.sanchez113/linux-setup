# Linux Setup

Scripts, config, etc. that I use for setting up a new Linux system.

**WARNING:** Some code may not have been tested. This is more for my own personal reference than anything since I don't always need to install each and every thing listed here. As a result, I just deal with any errors as they arise - if you decide to use this script, I expect you to do the same.

Steps if you really trust me for some reason:

```bash
git clone $REPO_URL
cd linux-setup

# For Ubuntu
./install-base-apt.sh

# If this is a Ubuntu desktop
./install-graphical-deps-apt.sh

# For RHEL
./install-base-yum.sh

# Base utilities/config
# This will take a while!
./install-base-utils

cp \.* $HOME

# Scaffold SSH config w/ useful options
mkdir $HOME/.ssh
chmod 600 $HOME/.ssh
cp .ssh/config $HOME/.ssh

# Log out, then log back in
# Configure the tools that were just installed
./configure-base-utils.sh
```

**TODO:** Add `pip install -r requirements-global.txt` to final script.

If desired, `install-infosec-tools.sh` can install some extra utilities. And no, I'm not proud of that script. It's an old script that I wrote a while back - I just wanted a starting point at the time. I'll likely end up trimming it to the most common/immediately useful tools.

## requirements-global.txt

This contains tools that I'd recommend installing across all Python installations for a basic development environment/starting point.

### requirements.txt

Reference point for Python packages that I've come across and found useful/neat.

Disclaimer: I haven't used all of these, and really, everything besides specific tools/utilities should be in a virtual environment anyway.

## Jupyter

This holds my Jupyter Lab setup. See its README for more info.

## Scripts

Any scripts I'd like to have available for reference.

## Templates

Templates for various languages/tools, e.g. bash script, `poetry` setup, CLI script.

## VS Code

Settings + setup scripts.
