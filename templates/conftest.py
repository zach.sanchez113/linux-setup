"""Configuration for pytest.

Place this in `tests/`.
"""

from __future__ import annotations

import os

import pytest
from _pytest import timing
from _pytest.terminal import TerminalReporter
from pytest import Config

pytest_plugins = "tests.fixtures"

# Enable debug logging during tests
if not os.environ.get("LOGURU_LEVEL"):
    os.environ["LOGURU_LEVEL"] = "DEBUG"

# Disable enqueue when running pytest (assuming the logging setup accounts for this environment variable)
os.environ["LOGURU_ENQUEUE"] = "False"


@pytest.fixture(autouse=True)
def example_fixture():
    """Run this before each test, e.g. remove logfiles generated during testing."""


class LoguruTerminalReporter(TerminalReporter):  # pyright: ignore
    """Subclass of pytest's TerminalReporter to customize output."""

    def __init__(self, reporter: TerminalReporter):
        TerminalReporter.__init__(self, reporter.config)

    def pytest_collection(self: TerminalReporter):
        """Write initial 'collecting ...' output, without erasing after collection and with an
        additional newline.
        """

        if self.isatty:
            if self.config.option.verbose >= 0:
                self.write("collecting ... \n", flush=True, bold=True)
                self._collect_report_last_write = timing.time()

        elif self.config.option.verbose >= 1:
            self.write("collecting ... \n", flush=True, bold=True)

    def pytest_runtest_logstart(self: TerminalReporter, nodeid: str, location: tuple[str, int | None, str]):
        """Ensure that the path is printed before the first test of a module starts running, with
        an additional newline.
        """

        line = self._locationline(nodeid, *location)
        self.write_ensure_prefix(line)
        self.ensure_newline()
        self.flush()


@pytest.hookimpl(trylast=True)
def pytest_configure(config: Config):
    """Allow plugins and conftest files to perform initial configuration."""

    # Get the standard terminal reporter plugin and replace it with ours if printing all stdout
    #
    # You want this because if ours doesn't get registered, the first log message will be written
    # right next to the test name
    #
    # NOTE: pytest-sugar is disabled because you'd get duplicate output if enabled
    if config.getvalue("capture") in ["no", "tee-sys"]:
        if config.pluginmanager.getplugin("sugar"):
            config.pluginmanager.unregister(name="sugar")
        else:
            print("WARNING: Couldn't find pytest-sugar for unregistering!")

        if not (standard_reporter := config.pluginmanager.getplugin("terminalreporter")):
            raise Exception("Failed to get the 'terminalreporter' plugin for override!")

        loguru_reporter = LoguruTerminalReporter(standard_reporter)
        config.pluginmanager.unregister(standard_reporter)
        config.pluginmanager.register(loguru_reporter, "terminalreporter")

    # Ensure we get colorful output even when capturing stdout
    if config.getvalue("capture") in ["sys", "tee-sys"]:
        os.environ["LOGURU_COLORIZE"] = str(True)
