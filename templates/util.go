/*
	These are utility functions I've written as a newbie working on a variety of projects.

	Some functions aren't super useful in most projects, but they've been included for my own personal
	reference.
*/

package zsanchez_util

import (
	"fmt"
	"io/fs"
	"os"
	"os/exec"
	"os/user"
	"path/filepath"
	"strings"
	"syscall"
	"time"
)

// See:
// https://stackoverflow.com/a/72367983/11832705
// https://codereview.stackexchange.com/a/79100
const (
	USER_R_PERMS  = 0b100000000
	USER_W_PERMS  = 0b010000000
	USER_X_PERMS  = 0b001000000
	GROUP_R_PERMS = 0b000100000
	GROUP_W_PERMS = 0b000010000
	GROUP_X_PERMS = 0b000001000
	OTHER_R_PERMS = 0b000000100
	OTHER_W_PERMS = 0b000000010
	OTHER_X_PERMS = 0b000000001
)

// Convert a slice of bytes to string, strip whitespace, and split by newline.
func SplitLinesBytes(input []byte) []string {
	return strings.Split(strings.TrimSpace(string(input[:])), "\n")
}

// Remove duplicates from a slice of strings.
func RemoveDuplicateStr(strSlice []string) []string {
	allKeys := make(map[string]bool)
	list := []string{}

	for _, item := range strSlice {
		if _, value := allKeys[item]; !value {
			allKeys[item] = true
			list = append(list, item)
		}
	}

	return list
}

// Filter a slice using a test function.
//
// NOTE: This requires Go 1.18+
func Filter[T any](ss []T, test func(T) bool) (ret []T) {
	for _, s := range ss {
		if test(s) {
			ret = append(ret, s)
		}
	}
	return
}

// Return a string representing the date and time in ISO 8601 format.
func Isoformat(t time.Time) string {
	return t.Format("2006-01-02T15:04:05+00:00")
}

// Return a string representing the current date and time (UTC) in ISO 8601 format.
func IsoformatUtcNow() string {
	return Isoformat(time.Now().UTC())
}

// Get the hostname.
func GetHostname() string {
	if hostname, err := os.Hostname(); err != nil {
		panic(fmt.Errorf("failed to get hostname: %v", err))
	} else {
		return hostname
	}
}

// Check if a command exists on the PATH.
func CommandExists(cmd string) bool {
	_, err := exec.LookPath(cmd)
	return err == nil
}

// Expand symlinks in a path.
// Returns the path unaltered if an error occurs (can happen if the path doesn't exist).
func ExpandSymlinks(path string) string {
	path = strings.TrimSpace(path)

	if resolvedPath, err := filepath.EvalSymlinks(path); err != nil {
		return path
	} else {
		return resolvedPath
	}
}

// Check if a file exists.
func FileExists(filename string) bool {
	filename = ExpandSymlinks(filename)

	if _, err := os.Stat(filename); err == nil {
		return true
	} else {
		return false
	}
}

// Get a file's mode.
func GetFileMode(filename string) fs.FileMode {
	filename = ExpandSymlinks(filename)

	if info, err := os.Stat(filename); err != nil {
		panic(fmt.Errorf("failed to stat %s: %v", filename, err))
	} else {
		return info.Mode()
	}
}

// Check if a file is readable.
func CheckFileReadable(path string) bool {
	path = ExpandSymlinks(path)

	if _, err := os.ReadFile(path); err != nil {
		return false
	} else {
		return true
	}
}

// Check if a file is writable by the owner.
func FileWritableByOwner(filename string) bool {
	filename = ExpandSymlinks(filename)
	return GetFileMode(filename)&USER_W_PERMS != 0
}

// Check if a file is readable by the owning group.
func FileReadableByGroup(filename string) bool {
	filename = ExpandSymlinks(filename)
	return GetFileMode(filename)&GROUP_R_PERMS != 0
}

// Check if a file is writable by the owning group.
func FileWritableByGroup(filename string) bool {
	filename = ExpandSymlinks(filename)
	return GetFileMode(filename)&GROUP_W_PERMS != 0
}

// Check if a file is readable by others.
func FileReadableByOthers(filename string) bool {
	filename = ExpandSymlinks(filename)
	return GetFileMode(filename)&OTHER_R_PERMS != 0
}

// Check if a file is writable by others.
func FileWritableByOthers(filename string) bool {
	filename = ExpandSymlinks(filename)
	return GetFileMode(filename)&OTHER_W_PERMS != 0
}

// Get the username of the user that owns a file.
func GetFileUsername(path string) string {
	path = ExpandSymlinks(path)

	info, _ := os.Stat(path)

	if stat, ok := info.Sys().(*syscall.Stat_t); ok {
		uid := int(stat.Uid)
		if user, err := user.LookupId(fmt.Sprint(uid)); err != nil {
			return ""
		} else {
			return user.Username
		}
	} else {
		return ""
	}
}

// Get the name of the group that owns a file.
func GetFileGroupName(path string) string {
	path = ExpandSymlinks(path)

	info, _ := os.Stat(path)

	if stat, ok := info.Sys().(*syscall.Stat_t); ok {
		gid := int(stat.Gid)
		if group, err := user.LookupGroupId(fmt.Sprint(gid)); err != nil {
			return ""
		} else {
			return group.Name
		}
	} else {
		return ""
	}
}
