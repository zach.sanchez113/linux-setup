#!/usr/bin/env bash

################################################################################
#
# A set of utility functions for bash that I've been collecting.
#
################################################################################

# Bash strict mode
# See: http://redsymbol.net/articles/unofficial-bash-strict-mode/
set -e
set -u
set -o pipefail
shopt -s failglob

# Write error to stdout then exit 1
#
# Args:
#   message
#
function err() {
  echo "${1}" >&2
  echo "aborting" >&2
  exit 1
}

# Check if command exists
#
# Args:
#   command to check existence of
#
function check_cmd() {
  command -v "${1:?no command supplied}" > /dev/null 2>&1
}

# Error out if a command doesn't exist
#
# Args:
#   command name
#
function need_cmd() {
  if ! check_cmd "${1:?no command supplied}"; then
    err "need '$1' (command not found)"
  fi
}

# Whether this script is being run as root
#
# Returns:
#   0 if true, 1 if false
#
function assert_root() {
  if [ $EUID -ne 0 ]; then
    err "This script must be run as root"
  fi
}

# Join an array by a delimiter
# Credit: https://stackoverflow.com/a/17841619/11832705
#
# Args:
#   delimiter
#   array to join
#
# Returns:
#   string of the array elements delimited by the delimiter
#
function join_by {

  local d=${1-} f=${2-}

  if shift 2; then
    printf %s "$f" "${@/#/$d}"
  fi

}

# List all binaries in $PATH
# Credit: https://unix.stackexchange.com/a/60823
#
function lspath () {

  shopt -s extglob

  local IFS pattern

  IFS='|'
  pattern="*@($*)*"
  IFS=':'

  for d in $PATH; do

    for x in "$d/"$pattern; do
      [ "$x" = "$d/$pattern" ] || echo "${x##*/}"
    done

  done | sort -u

  shopt -u extglob

}

# Print out the current date in ISO-8601 format
#
# Returns:
#   ISO-formatted date
#
function isodate() {
  date +%Y-%m-%dT%H:%M:%S%z
}

# Show the IP address for a network interface
# Credit: https://serverfault.com/a/911621
#
# Args:
#   Interface name
#
# Returns:
#   IP address of the interface
#
function nic_ip_addr() {
  ip -f inet addr show tun0 | sed -En -e 's/.*inet ([0-9.]+).*/\1/p'
}
