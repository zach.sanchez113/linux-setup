"""Configuration managed by Pydantic.

This is hardly the most comprehensive solution, but it's a starting point.
"""

from __future__ import annotations

import os
import sys
from typing import Any

import pytomlpp as toml
from appdirs import user_config_dir
from path import Path
from pydantic import BaseModel as PydanticBaseModel
from pydantic import ConfigDict, Field

model_config_immutable: ConfigDict = ConfigDict(
    # Whether arbitrary types are allowed for field types.
    arbitrary_types_allowed=True,
    # Whether to ignore, allow, or forbid extra attributes during model initialization.
    extra="forbid",
    # Whether models are faux-immutable.
    frozen=True,
    # Whether an aliased field may be populated by its name as given by the model attribute, as well as
    # the alias.
    populate_by_name=True,
    # By default, Pydantic will attempt to coerce values to the desired type when possible. Instead of
    # coercing data, raise an error.
    strict=True,
    # Whether to validate the data when the model is changed.
    validate_assignment=True,
    # Whether to validate default values during validation.
    validate_default=True,
    # Whether to validate the return value from call validators.
    validate_return=True,
)

model_config_mutable: ConfigDict = ConfigDict(
    # Whether arbitrary types are allowed for field types.
    arbitrary_types_allowed=True,
    # Whether to ignore, allow, or forbid extra attributes during model initialization.
    extra="forbid",
    # Whether models are faux-immutable.
    frozen=False,
    # Whether an aliased field may be populated by its name as given by the model attribute, as well as
    # the alias.
    populate_by_name=True,
    # By default, Pydantic will attempt to coerce values to the desired type when possible. Instead of
    # coercing data, raise an error.
    strict=True,
    # Whether to validate the data when the model is changed.
    validate_assignment=True,
    # Whether to validate default values during validation.
    validate_default=True,
    # Whether to validate the return value from call validators.
    validate_return=True,
)


class BaseModelReadOnly(PydanticBaseModel):
    """Read-only Pydantic model with typical config."""

    model_config = model_config_immutable


class BaseModelMutable(PydanticBaseModel):
    """Mutable Pydantic model with typical config."""

    model_config = model_config_mutable


class Configuration(BaseModelReadOnly):
    debug: bool = Field(default=False)


class ConfigurationFileNotFoundError(Exception):
    """Failed to locate the configuration file."""


def validate_dir(p: Any) -> bool:
    try:
        return Path(p).expand().isdir()
    except Exception:
        return False


def validate_file(p: Any) -> bool:
    try:
        return Path(p).expand().isfile() and Path(p).access(os.R_OK)
    except Exception:
        return False


def validate_file_executable(p: Any) -> bool:
    try:
        return validate_file(p) and Path(p).access(os.X_OK)
    except Exception:
        return False


def validate_file_or_empty(p: Any) -> bool:
    try:
        return not p or validate_file(p)
    except Exception:
        return False


def load_config(filename: str, walk_parent_dirs: bool = False) -> Configuration:
    # First, check current directory
    if walk_parent_dirs:
        current_directory: Path = Path("./").expand().abspath().parent

        while current_directory.parent != current_directory:
            if (config_file := current_directory / filename).isfile():
                return Configuration.model_validate(toml.loads(config_file.text()))
            else:
                current_directory: Path = current_directory.parent

    # Check user-specific config directory
    if (config_file := Path(user_config_dir()).expand().abspath() / filename).isfile():
        return Configuration.model_validate(toml.loads(config_file.text()))

    # Check global config directory (Linux-only)
    if sys.platform == "linux" and (config_file := Path(f"/etc/{filename}")).isfile():
        return Configuration.model_validate(toml.loads(config_file.text()))

    raise ConfigurationFileNotFoundError(f"Failed to locate configuration file {filename!r}!")


settings = load_config("example.toml")
