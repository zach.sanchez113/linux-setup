#!/usr/bin/env bash


################################################################################
#
# Overall references:
# - https://wiki.bash-hackers.org/
# - https://tldp.org/
#
# Control characters:
# https://en.wikipedia.org/wiki/Control_character#In_ASCII
#
# ANSI C quoting:
# https://www.gnu.org/software/bash/manual/html_node/ANSI_002dC-Quoting.html
#
# ASCII escape:
# https://en.wikipedia.org/wiki/ASCII#Printable_characters
#
################################################################################


#################################################
#
# Redirect all output to a logfile
#
# Credit: https://serverfault.com/a/103569/534704
#
#################################################

exec 3>&1 4>&2
trap 'exec 2>&4 1>&3' 0 1 2 3
exec 1>/var/log/example.log


#################################################
#
# set/shopt builtins
#
# Mainly pulled from rustup's installation script
# as of now
#
#################################################

# region

# exit upon error
set -e

# exit upon undefined variable
set -u

# pipe normally has return code of final instruction,
# and this allows failure to occur for ENTIRE pipe
set -o pipefail

# 0 for set, 1 for unset
shopt -q "$1"

# set
shopt -s "$1"

# unset
shopt -u "$1"

# endregion


#################################################
#
# Helpful functions
#
# Mainly pulled from rustup's installation script
# as of now
#
#################################################

# region

err() {
  echo "$1" >&2
  exit 1
}

need_cmd() {
  if ! check_cmd "$1"; then
    err "need '$1' (command not found)"
  fi
}

check_cmd() {
  command -v "$1" > /dev/null 2>&1
}

assert_nz() {
    if [ -z "$1" ]; then err "assert_nz $2"; fi
}

# Run a command that should never fail. If the command fails execution
# will immediately terminate with an error showing the failing
# command.
ensure() {
    if ! "$@"; then err "command failed: $*"; fi
}

abs_path() {
    local path="$1"
    # Unset CDPATH because it causes havok: it makes the destination unpredictable
    # and triggers 'cd' to print the path to stdout. Route `cd`'s output to /dev/null
    # for good measure.
    (unset CDPATH && cd "$path" > /dev/null && pwd)
}


# Interesting idea: instead of worrying about
# return values, just set a non-local variable
# RETVAL="$result" and access it from outside
#
example_retval() {
  local result="result"
  RETVAL="$result"
}

example_retval
my_var="$RETVAL"

# endregion


#################################################
#
# General
#
#################################################

# region

# print IFS
# https://unix.stackexchange.com/a/209802/365794
printf %q "$IFS"; echo

# ANSI C quoting
# https://www.gnu.org/software/bash/manual/html_node/ANSI_002dC-Quoting.html
echo $'hello world\n'

# ASCII escape
# https://en.wikipedia.org/wiki/ASCII#Printable_characters
echo -e '\x27'  # single quote (hex)
echo -e '\047'  # single quote (octal)
echo -e "\x22"  # double quote (hex)
echo -e "\042"  # double quote (octal)

# echo to stderr
echo "$1" 1>&2

## check for empty or unset string
[ -z "$1" ]
# or for just empty (likely don't want):
[ "$1" = "" ]

## default value
echo "${1:-default}"
# or to capture ONLY unset vars (likely don't want):
echo "${1-default}"

# Make sure we're root
if [ $EUID -ne 0 ]; then
  echo "You must be root!" 1>&2
  exit 1
fi

# Make sure a command is available
if ! command -v "$1" > /dev/null 2>&1; then
  echo "'$1' not found"
fi

# Number of params
echo "$#"

# Process tree
ps -auxwf
procs --tree

# use unset -v for portability
# https://wiki.bash-hackers.org/commands/builtin/unset#portability_considerations
unset -v example


## declare builtin
# https://www.gnu.org/software/bash/manual/html_node/Bash-Builtins.html
declare -i int
declare -l lowercase_str
declare -u uppercase_str
declare -r ro_var

# execute multiple commands w/ find
# (final semicolon to illustrate multiple find's in one go)
find / -name "whatever.txt" -type f -exec ls -lh {} \; -exec cat {} \;;

# endregion

#################################################
#
# Arrays
#
# https://wiki.bash-hackers.org/syntax/arrays
#
#################################################

# region

# Indexed array
example[0]="whatever"
example[1]="something"

# Print the array
# For difference between $@ and $*, see: https://wiki.bash-hackers.org/scripting/posparams#mass_usage
echo "${example[@]}"   # -> all elements individually quoted
echo "${example[*]}"   # -> all elements quoted as a whole
echo "${#example[0]}"  # -> length of first element
echo "${#example[@]}"  # -> number of elements
echo "${!example[@]}"  # -> indices in array

# ! WARNING
echo "${example}"  # -> default to first element, here "whatever"
example=()
example[1]="something"
echo "${example}"  # -> nothing at all

# Iteration
for ((i = 0; i <= ${#example[@]}; i++)); do
  echo "Element $i: '${example[i]}'"
done

for elem in "${example[@]}"; do
  echo "$elem"
done

# For an empty indexed array:
declare -a example=()

# Though to empty out a preexisting one, this is needed:
example=()

# To fill out indices ahead of time:
declare -a example=([1]="something" [2]="next")

# Associative array
declare -A example
example[whatever]="something"
echo "${example[whatever]}"
echo "${#example[elem]}"  # -> length of element 'elem'
echo "${!example[@]}"  # -> indices in array

# Alternatively:
declare -A example=([elem]="value" [another]="next")

# Delete a specific element
unset -v example[elem]

# endregion


#################################################
#
# Utilities
#
#################################################

# replace tabs w/ newlines
sed 's/\t\t*/\n/g'

# split by delimiter, print each component on newline
echo "${PATH}" | awk -F ':' 'BEGIN { OFS="\n"; }; { $1=$1; print 0$0; } '
