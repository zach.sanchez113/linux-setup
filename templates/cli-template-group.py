#!/usr/bin/env python3

"""Starter template for CLI scripts (multiple commands).

NOTE: Watch issue #18 and the PR for #89 since it may fix these bugs: https://github.com/ewels/rich-click/pull/92

TODO: Bug report for rich-click (#18):
_get_parameter_help should respect `show_default` in click.Context
Add the following condition: `or (getattr(ctx, "show_default", None) and isinstance(param, click.core.Option))`

TODO: Bug report for rich-click (maybe handled by #89):
APPEND_METAVARS_HELP doesn't show allowed range if isinstance(param.type, click.types._NumberRangeBase)
Add the code from generating `metavar` column in `rich_format_help`

TODO: Feature request for rich-click (#49):
Don't always have one line after the next, and markdown shouldn't necessarily toggle that behavior
"""

from __future__ import annotations

import os
import re
import sys
from textwrap import dedent
from typing import Any

import pssh.output
import psutil
import rich_click as click
from arrow import Arrow
from loguru import logger
from path import Path
from pssh.clients import ParallelSSHClient
from rich.console import Console
from rich.panel import Panel
from rich.table import Table

# Config for rich-click
click.rich_click.STYLE_HELPTEXT = ""
click.rich_click.SHOW_ARGUMENTS = True
click.rich_click.SHOW_METAVARS_COLUMN = False
click.rich_click.APPEND_METAVARS_HELP = True
click.rich_click.STYLE_OPTIONS_TABLE_PAD_EDGE = True
click.rich_click.COMMAND_GROUPS = {}

# Grab the Rich console
console = Console()

# Base logging configuration

# Default log message format (multiline to make it more readable)
LOG_FORMAT_DEFAULT = """
    <green>{time:YYYY-MM-DD HH:mm:ss}</green> | <level>{level: <8}</level>
    | <cyan>{name}</cyan>:<cyan>{function}</cyan>:<cyan>{line}</cyan> - <level>{message}</level>
"""

LOG_FORMAT = re.sub(r"\n", "", dedent(LOG_FORMAT_DEFAULT))

# If you don't want to show full module/function/line (this is better for standalone scripts)
LOG_FORMAT = "<green>{time:YYYY-MM-DD HH:mm:ss}</green> | <level>{level: <8}</level> | <level>{message}</level>"  # fmt: skip

logger.configure(
    handlers=[
        dict(sink=sys.stdout, level=os.environ.get("LOGURU_LEVEL", "INFO"), format=LOG_FORMAT),
    ],
    levels=[
        {"name": "INFO", "color": "<blue><bold>"},
        {"name": "DEBUG", "color": "<magenta><bold>"},
    ],
)


class LoguruCatchGroup(click.RichGroup):
    """Subclass of RichGroup that wraps commands in loguru's `logger.catch` context manager."""

    def __call__(self, *args: Any, **kwargs: Any):
        with logger.catch(onerror=lambda e: sys.exit(1), exclude=(click.ClickException, click.Abort)):
            # If used in combination with a library...
            # configure_logging(log_level="INFO")
            return super().__call__(*args, **kwargs)


class LoguruCatchCommand(click.RichCommand):
    """Subclass of RichCommand that wraps commands in loguru's `logger.catch` context manager."""

    def __call__(self, *args: Any, **kwargs: Any):
        with logger.catch(onerror=lambda e: sys.exit(1), exclude=(click.ClickException, click.Abort)):
            # If used in combination with a library...
            # configure_logging(log_level="INFO")
            return super().__call__(*args, **kwargs)


@click.group(cls=LoguruCatchGroup, context_settings=dict(help_option_names=["-h", "--help"]))
def cli():
    """Example command group for the CLI script."""


@cli.command(
    # cls=LoguruCatchCommand,
    no_args_is_help=True,
    context_settings=dict(
        show_default=True,
        help_option_names=["-h", "--help"],
    ),
)
@click.argument("arg", type=str)
@click.option("--debug", is_flag=True, help="Enable debug logging.")
def command(arg: str, debug: bool = False):
    """Example command for the CLI script."""

    if debug:
        logger.remove()
        logger.add(sink=sys.stdout, level="DEBUG", format=LOG_FORMAT)

    ...


if __name__ == "__main__":
    cli()
