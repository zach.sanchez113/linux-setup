package zsanchez_util

import (
	"errors"
	"io"
	"log/syslog"
	"os"

	"github.com/charmbracelet/lipgloss"
	"github.com/charmbracelet/log"
)

const (
	pink       = lipgloss.Color("212")
	purple     = lipgloss.Color("99")
	kindaWhite = lipgloss.Color("#FAFAFA")
)

// Logging config.
// TODO: Implement Windows Event Log
type LogConfig struct {
	// Enable debug logging.
	Debug bool
	// Log to stderr.
	LogStderr bool
	// File to log to.
	LogFile string
	// Log to syslog.
	Syslog    bool
	SyslogTag string
	// Log to Windows event log.
	EventLog bool
}

var AppLogConfig = LogConfig{
	LogStderr: true,
}

// Instantiate and return a logger.
func GetLogger(_config ...LogConfig) *log.Logger {
	var config LogConfig

	if len(_config) > 1 {
		panic(errors.New("more than one LogConfig was provided to GetLogger"))
	} else if len(_config) == 1 {
		config = _config[0]
	} else {
		config = AppLogConfig
	}

	logger := log.NewWithOptions(os.Stderr, log.Options{
		ReportTimestamp: true,
		TimeFormat:      "2006-01-02 15:04:05-07:00",
		ReportCaller:    true,
	})

	styles := log.DefaultStyles()

	styles.Levels[log.InfoLevel] = lipgloss.NewStyle().
		SetString("INFO    |").
		Foreground(lipgloss.Color("38"))

	styles.Levels[log.DebugLevel] = lipgloss.NewStyle().
		SetString("DEBUG   |").
		Foreground(purple)

	styles.Levels[log.WarnLevel] = lipgloss.NewStyle().
		SetString("WARNING |").
		Foreground(lipgloss.Color("226"))

	styles.Levels[log.ErrorLevel] = lipgloss.NewStyle().
		SetString("ERROR   |").
		Foreground(lipgloss.Color("160"))

	styles.Levels[log.FatalLevel] = lipgloss.NewStyle().
		SetString("FATAL   |").
		Foreground(kindaWhite).
		Background(lipgloss.Color("160"))

	logger.SetStyles(styles)

	if config.Debug {
		logger.SetLevel(log.DebugLevel)
	}

	var logFile *os.File
	var syslogHandle *syslog.Writer
	var err error

	if config.LogFile != "" {
		// Create the logfile if needed
		if exists, err := PathExists(config.LogFile); err != nil {
			panic(err)
		} else if !exists {
			f, err := os.Create(config.LogFile)
			if err != nil {
				panic(err)
			}
			f.Close()
		}

		logFile, err = os.OpenFile(config.LogFile, os.O_CREATE|os.O_WRONLY|os.O_APPEND, 0640)
		if err != nil {
			panic(err)
		}

		logger.SetOutput(logFile)
	}

	if config.Syslog {
		if config.SyslogTag == "" {
			panic(errors.New("syslog requested, but missing value for config.SyslogTag"))
		}

		syslogHandle, err = syslog.New(syslog.LOG_INFO|syslog.LOG_USER, config.SyslogTag)

		if err == nil {
			logger.SetOutput(syslogHandle)
		}
	}

	// Determine which sources are applicable here
	sources := []io.Writer{}

	if config.LogStderr {
		sources = append(sources, os.Stderr)
	}
	if logFile != nil {
		sources = append(sources, logFile)
	}
	if syslogHandle != nil {
		sources = append(sources, syslogHandle)
	}

	// Combine sources if needed
	// NOTE: Don't want to use io.MultiWriter if just a normal os.Stderr because it'll strip the colored output
	if len(sources) == 1 {
		logger.SetOutput(sources[0])
	} else {
		writer := io.MultiWriter(sources...)
		logger.SetOutput(writer)
	}

	return logger
}
