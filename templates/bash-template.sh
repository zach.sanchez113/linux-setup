#!/usr/bin/env bash

# Bash strict mode
# See: http://redsymbol.net/articles/unofficial-bash-strict-mode/
set -e
set -u
set -o pipefail
shopt -s failglob


#################################################
#
# Utility functions
#
#################################################

# Write error to stdout then exit 1
#
# Args:
#   message
#
function err() {
  echo "${1}" >&2
  echo "aborting" >&2
  exit 1
}

# Check if command exists
#
# Args:
#   command to check existence of
#
function check_cmd() {
  command -v "${1:?no command supplied}" > /dev/null 2>&1
}

# Error out if a command doesn't exist
#
# Args:
#   command name
#
function need_cmd() {
  if ! check_cmd "${1:?no command supplied}"; then
    err "need '$1' (command not found)"
  fi
}

# Whether this script is being run as root
#
# Returns:
#   0 if true, 1 if false
#
function assert_root() {
  if [ $EUID -ne 0 ]; then
    err "This script must be run as root"
  fi
}


#################################################
#
# Section name/notes/whatever
#
#################################################

# Main function
#
function main() {
  :
}

main
