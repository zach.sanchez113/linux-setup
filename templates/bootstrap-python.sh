#!/usr/bin/env bash

# Bash strict mode
# See: http://redsymbol.net/articles/unofficial-bash-strict-mode/
set -e
set -u
set -o pipefail
shopt -s failglob

# Do the thing
pip install --upgrade pip setuptool wheel poetry

# Defaults I usually want
# NOTE: This assumes you're in the same directory as pyproject.toml
poetry add \
  arrow@latest \
  attrs@latest \
  click@latest \
  loguru@latest \
  path@latest \
  pydantic@latest \
  pyrsistent@latest \
  pytomlpp@latest \
  rich@latest \
  rich-click@latest \
  typing-extensions@latest

# If using Jupyter
poetry add \
  jupyterlab@latest \
  ipython-autotime@latest \
  polars@latest \
  pandas@latest

# Base dev depencencies for autoformatting + testing
poetry add --group dev \
  pre-commit@latest \
  ruff@latest \
  pyright@latest

# For testing (not 100% necessary for smaller projects)
poetry add --group dev \
  pytest@latest \
  pytest-env@latest \
  pytest-mock@latest \
  pytest-sugar@latest \
  faker@latest

# For typing
# I usually use these outside of my base templates/utilities, but I forget these exist -> adding so I don't
poetry add --group dev \
  types-ipaddress \
  types-ldap3 \
  types-requests

poetry run pre-commit autoupdate
poetry run pre-commit install -f --install-hooks
