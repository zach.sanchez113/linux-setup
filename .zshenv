# Make colors work as expected (if the terminal supports it)
export COLORTERM=truecolor

# Load in rustup
[ -f "${HOME}/.cargo/env" ] && . "${HOME}/.cargo/env"

# Set this so golang works correctly
export GOPATH="${HOME}/go"

# Make .nvmrc do the thing
export NVM_AUTOLOAD=1

# Don't change prompt (useful for custom theming)
export PYENV_VIRTUALENV_DISABLE_PROMPT=1
export VIRTUAL_ENV_DISABLE_PROMPT=1

# Shut up warning about pyenv not working in non-interactive shells
# Required if .profile and/or .zprofile aren't set up correctly
# export ZSH_PYENV_QUIET=true

# Enable debug by default for dev purposes if desired
# LOGURU_LEVEL="DEBUG"

# If this should be used instead of IdentityAgent
# TODO: https://wiki.archlinux.org/title/SSH_keys#Start_ssh-agent_with_systemd_user
# export SSH_AUTH_SOCK=

# Process any extras needed for this specific box/env
if [ -d "${HOME}/.zshenv.d/" ]; then
  find "${HOME}/.zshenv.d/" -type f -print0 | while IFS= read -r -d $'\0' fn; do
    source "${fn}"
  done
fi
